<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%-- <%=session.getAttribute("userName") %>
 --%>
<%
	session = request.getSession(false);

	if (session != null && session.getAttribute("userName") != null
			&& !session.getAttribute("userName").equals("")) {
%>
<%@ page isELIgnored="false"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<script
	src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
<script
	src="http://cdn.jsdelivr.net/g/jquery@1,jquery.ui@1.10%28jquery.ui.core.min.js+jquery.ui.widget.min.js+jquery.ui.mouse.min.js+jquery.ui.sortable.min.js%29,angularjs@1.2,angular.ui-sortable"></script>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script
	src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.11.0/ui-bootstrap-tpls.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
<link
	href="${pageContext.servletContext.contextPath}/resources/css/index.css"
	rel="stylesheet">
<script
	src="${pageContext.servletContext.contextPath}/resources/index.js"></script>
<title>Edit Alert</title>
<%--<script type="text/javascript">
	$( document ).ajaxComplete(function() {
		//alert('hiiii');
		
		var searchBoxState = $('.search-result').is(':visible');
		if(searchBoxState==true){
			$('body').addClass('boxShowing');
		}
		else if(searchBoxState==false){
			$('body').addClass('boxHidden');
		}
		
	});
</script>--%>
</head>

<body>
	<div class="header">
		<h3 class="my-login-text">Edit Alert Interface</h3>
	</div>
	<div class="innaerpage-block">
		<div ng-app="myApp" ng-controller="myAppController"
			ng-init="initilizePathVariable('${pageContext.servletContext.contextPath}')">
			<div class="user-search"id "search-div">
				<div class="form-group">
					<input class="form-control" id="email" type="text" ng-model="email"
						placeholder="Enter Email">
				</div>
				<div class="form-group">
					<select ng-model="userWhiteLabel"
						class="whitelables-select form-control"
						ng-change="fetchProvider_FromWhitelables(userWhiteLabel)"
						ng-options="key as key for (key, value) in  myMainModel.whitelabel_map">
					</select>
				</div>
				<div class="form-group">

					<script>
						$("input").keypress(function(event) {
							if (event.which == 13) {
								$("#submit").click();
							}
						});
					</script>

					<button class="button btn btn-default" id="submit"
						ng-click="checkvalidation()">Search</button>
				</div>
				<div id="searchwith_provider" class="serach-with-provider"
					ng-show="false">
					<label>Search With Provider: </label> <input type="checkbox"
						ng-model="searchWithProvider" ng-true-value="true"
						ng-false-value="false"
						ng-change="search_WithProvider(searchWithProvider)" />
				</div>

				<div ng-show="searchWithProvider">
					<li class="providers_li_class"
						ng-repeat="provider in providersArray"><input type="checkbox"
						ng-model="provider.active" ng-true-value="true"
						ng-false-value="false" id="{{provider.provider}}"
						ng-change="providerIsCheckedOrNot(provider)" /> {{
						provider.provider }}</li>
				</div>

				<div ng-show="isAnyProviderFound && searchWithProvider">
					<label>Select All: </label> <input type="checkbox"
						ng-model="selectAll" ng-true-value="true" ng-false-value="false"
						ng-change="selectAll_Providers(selectAll)">
				</div>
				<label class="update-text"
					ng-show="!isAnyProviderFound && searchWithProvider">No
					Providers Found!!</label>

			</div>
			<div class="no-result-found-div"
				ng-show="noUserFoundLable&&!showUserDetail">
				<label id="no_data_label" class="no-result-text">No Result
					Found!</label>
			</div>
			<div class="search-result"
				ng-show="showUserDetail&&!noUserFoundLable">
				<div class="center-user-info">
					<div class="user-info col-md-6 col-lg-6 col-sm-12"
						ng-repeat="userModel in usersModelData">
						<div class="inner-user">
							<label class="label-text">{{userModel.user_whitelable}}</label>
							<!-- <label class="label-text">User Information</label> <br> -->

							<div class="inner-user-info">
								<div class="form-group">
									<label class="placeholder-label col-md-4">Email</label>
									<div class="col-md-8">
										<input class="form-control"
											id="{{userModel.email+'-'+userModel.documentId}}" type="text"
											ng-model="userModel.email" placeholder="Email.">
									</div>
								</div>

								<div class="form-group">
									<label class="placeholder-label col-md-4">Keyword</label>
									<div class="col-md-8">
										<input class="edit-text form-control"
											id="{{userModel.keyword+'-'+userModel.documentId}}"
											type="text" ng-model="userModel.keyword"
											placeholder="Keyword">
									</div>
								</div>

								<div class="form-group">
									<label class="placeholder-label col-md-4">Location</label>
									<div class="col-md-8">
										<input class="edit-text form-control"
											id="{{userModel.location+'-'+userModel.documentId}}"
											type="text" ng-model="userModel.location"
											placeholder="Location">
									</div>
								</div>

								<div class="form-group">
									<label class="placeholder-label col-md-4">City</label>
									<div class="col-md-8">
										<input class="edit-text form-control"
											id="{{userModel.city+'-'+userModel.documentId}}" type="text"
											ng-model="userModel.city" placeholder="City">
									</div>
								</div>

								<div class="form-group">
									<label class="placeholder-label col-md-4">State</label>
									<div class="col-md-8">
										<input class="edit-text form-control"
											id="{{userModel.state+'-'+userModel.documentId}}" type="text"
											ng-model="userModel.state" placeholder="State">
									</div>
								</div>

								<div class="form-group">
									<label class="placeholder-label col-md-4">Frequency</label>
									<div class="col-md-8">
										<select id="{{userModel.user_id+'-'+userModel.documentId}}"
											ng-model="userModel.frequency"
											class="frequency-select form-control"
											ng-options="frequency as frequency for frequency in frequencyArray">
										</select>
									</div>
								</div>

								<div class="form-group">

									<label class="placeholder-label col-md-4">Radius</label>
									<div class="col-md-8">
										<input class="edit-text form-control"
											id="{{userModel.radius+'-'+userModel.documentId}}"
											type="text" ng-model="userModel.radius" placeholder="Radius">
									</div>
								</div>

								<div class="form-group">
									<button class="button update" id="update"
										ng-click="updateUserInfo(userModel)">Update</button>
									<button class="button delete" id="delete"
										ng-show="isDeleteButton_show"
										ng-click="deleteUserInfo(userModel)">DELETE</button>
								</div>
								<div>
									<label id="status_label" class="update-text"
										ng-show="userModel.showUpdatedLabel">Successfully
										updated!!</label>
								</div>
							</div>
						</div>

					</div>
				</div>

			</div>

			<div id="startpageLoadingGif" class="modal fade" role="dialog">
				<div class="modal-dialog"
					style="width: 150px; height: 150px; margin-top: 20px">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-body" style="">
							<div class="row" style="text-align: center;">
								<label> Please wait we are processing ! </label> <img
									src="<c:url value='/images/startpageLoadingGif.gif'/>" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer"></div>
</body>
</html>
<%
	} else {
		response.sendRedirect(request.getContextPath() + "/");
	}
%>

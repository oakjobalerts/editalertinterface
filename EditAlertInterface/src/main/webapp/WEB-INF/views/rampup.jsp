<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page isELIgnored="false"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script> -->
<!-- <script src="http://cdn.jsdelivr.net/g/jquery@1,jquery.ui@1.10%28jquery.ui.core.min.js+jquery.ui.widget.min.js+jquery.ui.mouse.min.js+jquery.ui.sortable.min.js%29,angularjs@1.2,angular.ui-sortable"></script> -->

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script
	src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.11.0/ui-bootstrap-tpls.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
	 -->


<!-- Jquery -->
<script src="https://code.jquery.com/jquery-1.12.3.min.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.9/angular.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<!-- High Charts -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.11.0/ui-bootstrap-tpls.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet"
	href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

<script type="text/javascript"
	src="http://cdnjs.cloudflare.com/ajax/libs/lodash.js/2.4.1/lodash.min.js"></script>
<script type="text/javascript"
	src="http://cdnjs.cloudflare.com/ajax/libs/highlight.js/8.0/highlight.min.js"></script>
<script type="text/javascript"
	src="https://rawgit.com/pc035860/angular-highlightjs/master/angular-highlightjs.js"></script>
<script type="text/javascript"
	src="http://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.10.0/ui-bootstrap-tpls.js"></script>


<link
	href="${pageContext.servletContext.contextPath}/resources/css/rampup.css"
	rel="stylesheet">
<script
	src="${pageContext.servletContext.contextPath}/resources/rampup.js"></script>


<link
	href="${pageContext.servletContext.contextPath}/resources/css/xeditable.css"
	rel="stylesheet">
<script
	src="${pageContext.servletContext.contextPath}/resources/xeditable.js"></script>

<title>Rampup</title>
</head>
<body>
	<div class="header">
		<h3 class="headers-text">Rampup Interface</h3>
	</div>

	<div class="innaerpage-block container">
		<div ng-app="rampup" ng-controller="rampupController"
			ng-init="initilizePathVariable('${pageContext.servletContext.contextPath}')">


			<form class="form-inline">
				<div class="form-group">
					<label for="datepicker_startdate">Date : </label> <input
						class="date-picker-style" ng-model="dateSelected" type="text"
						id="datepicker_startdate" ng-required="true">

					<button class="button btn btn-success btn-lg" id="Go"
						ng-click="getAllrampupData(dateSelected)">Go</button>

				</div>
			</form>



			<div class="text-center p-t-30"></div>

			<div class="table_div table-wrapper"
				ng-init="getAllrampupData(dateSelected)">



				<table class="tableClass table table-striped rampupMain">
					<thead>
						<tr>
							<th>ProcessName</th>
							<th>Advertisement</th>
							<th>Rampup Type</th>
							<th>Rampup Count</th>
							<th>Esp</th>
							<th>Status</th>
						</tr>
					</thead>
					<tr ng-repeat="rampupModel in rampupProcessList">


						<td ng-click="setClickRampupModel(rampupModel)">{{
							rampupModel.processName }} <!-- <div class="slideTable">
								
							</div> -->
						</td>

						<td>{{ rampupModel.rampup_advertisement }}</td>
						<td><select ng-model="rampupModel.selected_ramp_type"
							class="whitelables-select form-control"
							ng-options="item for item in rampupModel.rampup_types"
							ng-change="isUpdateRequired(rampupModel)">

						</select></td>
						<td><a href="#" editable-text="rampupModel.ramp_users_count"
							e-ng-change="isUpdateRequired(rampupModel)">{{
								rampupModel.ramp_users_count || 'empty' }}</a></td>
						<td><select ng-model="rampupModel.selected_user_domain_type"
							class="whitelables-select form-control"
							ng-options="item for item in rampupModel.user_domain_type"
							ng-change="isUpdateRequired(rampupModel)">

						</select></td>

						<td><select ng-model="rampupModel.selected_rampup_status"
							class="whitelables-select form-control"
							ng-options="item for item in rampupStatus"
							ng-change="isUpdateRequired(rampupModel)">

						</select></td>
					</tr>


				</table>
			</div>

			<div class="text-center p-t-30">
				<button class="button btn btn-success btn-lg" id="update"
					ng-click="updateRampupInformation(rampupProcessList)">Update</button>
				<button class="button btn btn-success btn-lg" id="addnew"
					ng-click="addNewRecord()">Add New</button>
			</div>


			<!-- 	<div id="detail-dialog" class="modal fade" role="dialog">
				<div class="modal-dialog modal-lg">
					Modal content
					<div class="modal-content">
						<div class="modal-body" style="">
							<div class="row" style="text-align: center;"
								class="table_div table-wrapper">

								<table class="tableClass table table-striped rampupMain">
									<thead>
										<tr class="text-center-th">
											<th>DomainName</th>
											<th>Gmail</th>
											<th>Yahoo</th>
											<th>Aol</th>
											<th>Other</th>
										</tr>
									</thead>
									<tr class="text-center"
										ng-repeat="(domain, dataModel) in clickRampupModel.domain_wise_map">
										<td>{{domain}}</td>
										<td>{{dataModel['Gmail']}}</td>
										<td>{{dataModel['Yahoo']}}</td>
										<td>{{dataModel['Aol']}}</td>
										<td>{{dataModel['Other']}}</td>
									</tr>
								</table>

							</div>
						</div>
					</div>
				</div>
			</div> -->





			<div id="detail-dialog" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">{{clickRampupModel.processName}}</h4>
						</div>
						<div class="modal-body">
							<div class="row" style="text-align: center;"
								class="table_div table-wrapper">

								<table class="tableClass table table-striped rampupMain">
									<thead>
										<tr class="text-center-th">
											<th>DomainName</th>
											<th>Gmail</th>
											<th>Yahoo</th>
											<th>Aol</th>
											<th>Other</th>
										</tr>
									</thead>
									<tr class="text-center"
										ng-repeat="(domain, dataModel) in clickRampupModel.domain_wise_map">
										<td>{{domain}}</td>
										<td>{{dataModel['Gmail']}}</td>
										<td>{{dataModel['Yahoo']}}</td>
										<td>{{dataModel['Aol']}}</td>
										<td>{{dataModel['Other']}}</td>
									</tr>
								</table>

							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="button btn btn-success btn-lg"
								data-dismiss="modal">Cancel</button>
						</div>
					</div>

				</div>
			</div>



			<div id="add-new-dialog" class="modal fade" role="dialog"
				backdrop="static">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">×</button>
							<h4 class="modal-title">Add New Record</h4>
						</div>
						<div class="modal-body">
							<form>
								<div class="col-md-12">
									<div class="row">
									<div class="col-md-6">
											<div class="form-group">
												<label for="email">Advertisement</label> <select
													ng-model="addNewRecordModel.rampup_advertisement"
													class="whitelables-select form-control"
													ng-options="item for item in addNewRecordModel.new_rampup_advertisement">

												</select>
											</div>
										</div>
										<!-- <div class="col-md-1">
											<div class="form-group">
												<label for="email">+</label>
											</div>
										</div> -->
										<div class="col-md-6">
											<div class="form-group">
												<label for="email">ProcessName:</label><select
													ng-model="addNewRecordModel.processName"
													class="whitelables-select form-control"
													ng-options="key as key for (key, value) in  addNewRecordModel.whitelable_table_map">

												</select>
											</div>
										</div>


										<div class="col-md-6">
											<div class="form-group">
												<label for="email">Rampup Count</label> <select
													ng-model="addNewRecordModel.ramp_users_count"
													class="whitelables-select form-control"
													ng-options="item for item in rampupCountArray">

												</select>
											</div>
										</div>

										<!-- <div class="col-md-1">
											<div class="form-group">
												<label for="email">+</label>
											</div>
										</div> -->

										<div class="col-md-6">
											<div class="form-group">
												<label for="email">Rampup Type</label> <select
													ng-model="addNewRecordModel.ramp_up_type"
													class="whitelables-select form-control"
													ng-options="item for item in addNewRecordModel.rampup_types">

												</select>
												<!-- <input type="email"
													class="form-control" id="email"> -->
											</div>
										</div>

										

										<div class="col-md-6">
											<div class="form-group">
												<label for="email">Esp</label> <select
													ng-model="addNewRecordModel.selected_user_domain_type"
													class="whitelables-select form-control"
													ng-options="item for item in addNewRecordModel.user_domain_type">

												</select>
											</div>
										</div>

										<div class="col-md-5">
											<div class="form-group">
												<label for="email">Start Date:</label> <input
													class="date-picker-style"
													ng-model="rampup_start_dateSelected" type="text"
													id="rampup_start_dateSelected" ng-required="true">
											</div>
										</div>

									</div>
								</div>
								<div class="clearfix"></div>

							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="button btn btn-success btn-lg"
								data-dismiss="modal"
								ng-click="addNewRecordOfRampup(addNewRecordModel)">Add</button>
							<button type="button" class="button btn btn-success btn-lg"
								data-dismiss="modal">Cancel</button>
						</div>
					</div>

				</div>
			</div>

			<!-- 			<div id="add-new-dialog" class="modal fade" role="dialog"></div>
 -->

			<div id="responsealert" class="modal fade" role="dialog">
				<div class="modal-dialog modal-sm" style="margin-top: 100px">
					<!-- Modal content-->
					<div class="modal-content">

						<div class="modal-body" style="">
							<div class="row" style="text-align: center;">
								<label> {{responseData}} </label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Response Alert Modal Start -->



		<div id="startpageLoadingGif" class="modal fade" role="dialog">
			<div class="modal-dialog"
				style="width: 150px; height: 150px; margin-top: 20px">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-body" style="">
						<div class="row" style="text-align: center;">
							<label> Please wait we are processing ! </label> <img
								src="<c:url value='/images/startpageLoadingGif.gif'/>" />
						</div>
					</div>
				</div>
			</div>
		</div>


	</div>


	<div class="footer"></div>

</body>
</html>
var myApp = angular.module('myApp', []);

myApp
		.controller(
				"myAppController",
				function($scope, $http, $timeout, $filter, $window) {

					$scope.initilizePathVariable = function(pathVariable) {

						$scope.email = "";
						$scope.keyword = "";
						$scope.location = "";
						$scope.radius = "";
						$scope.number_of_provider_selected = "";

						$scope.showUserDetail = false;
						$scope.showUpdatedLabel = false;
						$scope.noUserFoundLable = false;
						$scope.searchWithProvider = false;
						$scope.isAnyProviderFound = false;
						$scope.isDeleteButton_show = false;
						$scope.usersModelData = [];
						$scope.frequencyArray = [];

						$scope.frequencyArray.push("Daily");
						$scope.frequencyArray.push("Weekly");
						$scope.frequencyArray.push("Monthly");
						$scope.frequencySelected = $scope.frequencyArray[0];

						$scope.selectAll = false;
						$scope.userWhiteLabel = "All";
						$scope.userProvider = "DECKIN";

						$scope.loadingImageText = "Please wait we are processing ! ";

						$scope.userModel = {};
						$scope.userModel.showUpdatedLabel = false;
						$scope.providersArray = [];

						// ===========================//=======================
						$scope.myMainModel = {};
						$scope.myMainModel.whitelabel_map = {};
						$scope.myMainModel.advertisement_map = {};

						// =======================//=========================

						$scope.provider_select_for_searching = [];

						// console.log("$scope.provider_select_for_searching"
						// + $scope.provider_select_for_searching)

						$scope.pathVariable = pathVariable;

						$('#startpageLoadingGif').modal({
							 backdrop : 'static',
							 keyboard : false
						});
						

						var response = $http.get($scope.pathVariable
								+ "/whitelables");

						response
								.success(function(data, status, headers, config) {

									$('#startpageLoadingGif').modal('hide');

									// console.log("Get all whitelables");

									// $scope.whitelables_tableHashmap = data;

									$scope.myMainModel = data;
									$scope.whiteLables = [];

									// =============================//================
									$scope.hashMapIterator_Function();

								});
						response.error(function(data, status, headers, config) {
							$('#startpageLoadingGif').modal('hide');
							alert(data);
						});

					}
					// =================================//===============================================
					$scope.hashMapIterator_Function = function() {
						$scope.firstValueFlag = true;

						// iteratinog hash map in angular
						angular.forEach($scope.myMainModel.whitelabel_map,
								function(value, key) {
									if ($scope.firstValueFlag) {
										// console.log(key);
										$scope.userWhiteLabel = key;
										$scope.firstValueFlag = false;
									}

								})
						$scope
								.fetchProvider_FromWhitelables($scope.userWhiteLabel);
					}

					// ==========================================//======================================
					// here we are fetching advertisement as per the
					// whtitelables
					$scope.fetchProvider_FromWhitelables = function(
							userWhiteLabel) {
						$scope.providersArray = [];
						// console.log(userWhiteLabel)
						$scope.advertisement_String = $scope.myMainModel.whitelabel_map[userWhiteLabel];

						$scope.myLocal_Array = [];
						$scope.myLocal_Array = $scope.advertisement_String
								.split("|");

						for (var i = 0; i < $scope.myLocal_Array.length; i++) {

							$scope.providerModel = {};
							$scope.providerModel.active = false;
							$scope.providerModel.provider = $scope.myLocal_Array[i];
							// console.log($scope.myLocal_Array[i]);
							$scope.providersArray.push($scope.providerModel);

						}

						$scope.selectAll = false;
						$scope.searchWithProvider = false;

						// console.log("selectAll=" + $scope.selectAll)
						// console.log("searchWithProvider="
						// + $scope.searchWithProvider)

						$scope.search_WithProvider($scope.searchWithProvider);

					}

					$scope.search_WithProvider = function(searchWithProvider) {

						for (var i = 0; i < $scope.providersArray.length; i++) {

							if ($scope.providersArray[i].provider == "") {
								// here we are removing the blank object from
								// array
								$scope.providersArray.splice(i, 1);

							}

						}

						if (searchWithProvider == "false") {

							$scope.selectAll = false;
							$scope.isAnyProviderFound = false;

							for (var i = 0; i < $scope.providersArray.length; i++) {

								$scope.providerModel = $scope.providersArray[i];
								$scope.providerModel.active = false;
								$scope.providersArray[i] = $scope.providerModel;

							}
						} else {
							if ($scope.providersArray.length == "0") {
								$scope.isAnyProviderFound = false;

							} else {
								$scope.isAnyProviderFound = true;

							}
						}
						// console.log("isAnyProviderFound="
						// + $scope.isAnyProviderFound);

					}
					// ============================================//=======================================
					$scope.providerIsCheckedOrNot = function(provider) {

						// console.log("provider is=" + provider.provider);
						// console.log("provider is selected=" +
						// provider.active);
						$scope.isAnySelected = false;

						for (var i = 0; i < $scope.providersArray.length; i++) {
							if ($scope.providersArray[i].active == "true") {
								$scope.isAnySelected = true;
								break;

							}

						}
						if ($scope.isAnySelected == false) {
							$scope.selectAll = false;

						}

					}
					// ============================================//===========================================
					$scope.selectAll_Providers = function(selectAll) {

						// console.log("selectAll=" + selectAll);

						if (selectAll == "true") {

							for (var i = 0; i < $scope.providersArray.length; i++) {

								$scope.providerModel = $scope.providersArray[i];
								$scope.providerModel.active = "true";
								$scope.providersArray[i] = $scope.providerModel;

							}

							$scope.provider_select_for_searching = angular
									.copy($scope.providersArray);

						} else {
							for (var i = 0; i < $scope.providersArray.length; i++) {

								$scope.providerModel = $scope.providersArray[i];
								$scope.providerModel.active = false;
								$scope.providersArray[i] = $scope.providerModel;

							}
							$scope.provider_select_for_searching = angular
									.copy($scope.providersArray);

						}

					}

					$scope.checkvalidation = function() {

						// console.log("Checking user in db")

						var re = /^(([^<>(){}!#$%^&*+=|\\'\/?~`[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>(){[\]\.,;:\s@\"]+\.)+[^<>(){}!#$%^&*+=|\\'\/?~`[\]\.,;:\s@\"]{2,})$/i;

						if (!$scope.email.match(re)) {
							var email = document.getElementById('email');
							email.style.backgroundColor = "#ff6666";
							return false;
						}

						
						document.getElementById('email').style.backgroundColor = "#ffffff";
						$('#startpageLoadingGif').modal({
							backdrop : 'static',
							 keyboard : false
						});
						$scope.number_of_provider_selected = "";
						for (var i = 0; i < $scope.providersArray.length; i++) {

							if ($scope.providersArray[i].active == "true") {
								// here we are adding only those which are
								// selected
								// $scope.provider_select_for_searching
								// .push($scope.providersArray[i].provider);
								//								
								$scope.number_of_provider_selected = $scope.number_of_provider_selected
										+ ""
										+ $scope.providersArray[i].provider
										+ ",";

							}

						}
						// console.log("$scope.number_of_provider_selected"
						// + $scope.number_of_provider_selected);

						// var response = $http.get($scope.pathVariable
						// + '/search-user/?email=' + $scope.email
						// + '&keyword=' + $scope.keyword + '&location='
						// + $scope.location + '&frequency='
						// + $scope.frequency + '&radius=' + $scope.radius
						// + '&whitelabel=' + $scope.userWhiteLabel
						// + '&providers='
						// + $scope.number_of_provider_selected);

						var response = $http.get($scope.pathVariable
								+ '/complete-user-search/?email='
								+ $scope.email + '&whitelabel='
								+ $scope.userWhiteLabel + '&providers='
								+ $scope.number_of_provider_selected);

						response
								.success(function(data, status, headers, config) {

									$('#startpageLoadingGif').modal('hide');
									// console.log(data);
									$scope.usersModelData = data;
									if ($scope.usersModelData.length == 0) {
										$scope.showUserDetail = false;
										$scope.noUserFoundLable = true;

									} else {

										$scope.showUserDetail = true;
										$scope.noUserFoundLable = false;

										for (var i = 0; i < $scope.usersModelData.length; i++) {

											if ($scope.usersModelData[i].frequency == "1") {
												$scope.usersModelData[i].frequency = "Daily";

											} else if ($scope.usersModelData[i].frequency == "3") {
												$scope.usersModelData[i].frequency = "Weekly";

											} else if ($scope.usersModelData[i].frequency == "4") {
												$scope.usersModelData[i].frequency = "Monthly";

											}
										}

									}

								});
						response.error(function(data, status, headers, config) {
							$('#startpageLoadingGif').modal('hide');
//							alert(data);
							$scope.showUserDetail = false;
							$scope.noUserFoundLable = true;
							
							
						});

					}

					$scope.updateUserInfo = function(userModel) {

						if ($scope.emailValidator(userModel) == true) {

							document.getElementById(userModel.email + "-"
									+ userModel.documentId).style.backgroundColor = "#ffffff";

							if ($scope
									.keyword_And_Location_Validator(userModel) == true) {

								document.getElementById(userModel.keyword + "-"
										+ userModel.documentId).style.backgroundColor = "#ffffff";

								document.getElementById(userModel.location
										+ "-" + userModel.documentId).style.backgroundColor = "#ffffff";

								$('#startpageLoadingGif').modal({
									backdrop : 'static',
									 keyboard : false
								});
								
								

								// $('#startpageLoadingGif').modal({
								// backdrop : 'static',
								// keyboard : false
								// });

								var response = $http.get($scope.pathVariable
										+ '/update-user/?email='
										+ userModel.email + '&keyword='
										+ userModel.keyword + '&zipcode='
										+ userModel.location + '&frequency='
										+ userModel.frequency + '&radius='
										+ userModel.radius + '&id='
										+ userModel.user_id + '&city='
										+ userModel.city + '&state='
										+ userModel.state + '&whitelabel='
										+ $scope.userWhiteLabel
										+ '&table_name='
										+ userModel.user_table_name

								);

								response.success(function(data, status,
										headers, config) {

									$('#startpageLoadingGif').modal('hide');
									userModel.showUpdatedLabel = true;
									// console.log("successfully updated")

								});

								response.error(function(data, status, headers,
										config) {
									$('#startpageLoadingGif').modal('hide');
									alert(data);
								});

							} else {
								return false;
							}
						} else {
							return false;
						}

						// console.log(userModel.user_id);
						// console.log(userModel.keyword);
						// $scope.loadingImageText = "Please wait we are
						// updating ! ";
						//
						// if (userModel.email != '') {
						// }

					}

					// =========================================//===========================================
					$scope.emailValidator = function(userModel) {

						// console.log("document id of email=" + userModel.email
						// + "-" + userModel.documentId)

						// console.log("email validator is running")

						
						var re = /^(([^<>(){}!#$%^&*+=|\\'\/?~`[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>(){[\]\.,;:\s@\"]+\.)+[^<>(){}!#$%^&*+=|\\'\/?~`[\]\.,;:\s@\"]{2,})$/i;
						if (!userModel.email.match(re)) {
							var email = document.getElementById(userModel.email
									+ "-" + userModel.documentId);
							email.style.backgroundColor = "#ff6666";

							document.getElementById(
									userModel.email + "-"
											+ userModel.documentId).focus();

							return false;
						}

							document.getElementById('email').style.backgroundColor = "#ffffff";
						return true;
						// $('#startpageLoadingGif').modal({
						//
						// });

					}

					$scope.keyword_And_Location_Validator = function(userModel) {

						// console.log("keyword_And_Location_Validator")

						if (userModel.keyword == "") {

							var email = document
									.getElementById(userModel.keyword + "-"
											+ userModel.documentId);
							email.style.backgroundColor = "#ff6666";

							document.getElementById(
									userModel.keyword + "-"
											+ userModel.documentId).focus();

							document.getElementById(userModel.email + "-"
									+ userModel.documentId).style.backgroundColor = "#ffffff";

							return false;

						} else if (userModel.location == "") {
							document.getElementById(userModel.keyword + "-"
									+ userModel.documentId).style.backgroundColor = "#ffffff";

							var email = document
									.getElementById(userModel.location + "-"
											+ userModel.documentId);
							email.style.backgroundColor = "#ff6666";

							document.getElementById(
									userModel.location + "-"
											+ userModel.documentId).focus();

							return false;

						}
						document.getElementById(userModel.location + "-"
								+ userModel.documentId).style.backgroundColor = "#ffffff";

						return true;
						// $('#startpageLoadingGif').modal({
						//
						// });

					}

					// =======================================//=============================================
					$scope.deleteUserInfo = function(userModel) {

						// console.log("user going to delete=" + userModel.email
						// + " from table=" + userModel.user_table_name);
						// checking the useremail befor deletion
						$scope.emailValidator(userModel);

						if ($scope.emailValidator(userModel) == true) {
							$('#startpageLoadingGif').modal({
								backdrop : 'static',
								 keyboard : false
							});

							var response = $http.get($scope.pathVariable
									+ '/delete-user/?email=' + userModel.email
									+ '&id=' + userModel.user_id
									+ '&table_name='
									+ userModel.user_table_name

							);

							response
									.success(function(data, status, headers,
											config) {

										$('#startpageLoadingGif').modal('hide');
										// console.log("successfully updated")

										// console
										// .log("deletetion result="
										// + data);

										if (data == "true") {

											// console.log("successfully
											// deleted"
											// + userModel.user_id);

											// here we are updating the current
											// array

											for (var i = 0; i < $scope.usersModelData.length; i++) {

												if ($scope.usersModelData[i].user_id == userModel.user_id) {
													// here we are removing the
													// blank object from
													// array
													$scope.usersModelData
															.splice(i, 1);

													// console
													// .log("successfully
													// updated the array="
													// + userModel.user_id);
												}

											}

											if ($scope.usersModelData.length == 0) {
												$scope.showUserDetail = false;
												$scope.noUserFoundLable = true;

											} else {
												$scope.showUserDetail = true;
												$scope.noUserFoundLable = false;

											}

										}

									});

							response.error(function(data, status, headers,
									config) {
								$('#startpageLoadingGif').modal('hide');
								alert(data);
							});
						} else {
							return false;
						}

					}

				});
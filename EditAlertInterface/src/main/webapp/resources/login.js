var myLoginApp = angular.module("loginApp", []);

myLoginApp.controller("loginController", function($scope, $http, $timeout,
		$filter) {

	$scope.userModel = {};
	$scope.userModel.userName = "";
	$scope.userModel.userPassword = "";
	$scope.isUserInValid = false;

	$scope.initilizePathVariable = function(pathVariable) {
		$scope.pathVariable = pathVariable;

	}

	$scope.loginuser = function(userModel) {

		console.log(userModel.usloginApperName);
		console.log(userModel.userPassword);

		if (userModel.userName.length == 0) {

			var userName = document.getElementById('userName');
			userName.style.backgroundColor = "#ff6666";
			return false;

		} else if (userModel.userPassword.length == 0) {
			var password = document.getElementById('userPassword');
			password.style.backgroundColor = "#ff6666";
			return false;

		} else {
			var userName = document.getElementById('userName');
			var password = document.getElementById('userPassword');
			userName.style.backgroundColor = "#ffffff";
			password.style.backgroundColor = "#ffffff";
			console.log("both entry found")

			$('#startpageLoadingGif').modal({
				backdrop : 'static',
				 keyboard : false
			});

			var response = $http.get($scope.pathVariable
					+ '/user-login/?userName=' + $scope.userModel.userName
					+ '&userPassword=' + $scope.userModel.userPassword);

			response.success(function(data, status, headers, config) {

				// if user is invalid
				console.log(data)
				if (data == 'true') {
					$scope.isUserInValid = false;
					console.log("data is true")
					$scope.userModel.userName = "";
					$scope.userModel.userPassword = "";
					// here we are redirecting to home page
					window.location = $scope.pathVariable + "/home"
					$('#startpageLoadingGif').modal('hide');

				}else if(data.trim()=='rampup'){
					
					window.location = $scope.pathVariable + "/rampup-page"
					$('#startpageLoadingGif').modal('hide');
					
				}
				else {
					console.log("data is false")
					$scope.userModel.userName = "";
					$scope.userModel.userPassword = "";
					$scope.isUserInValid = true;
					$('#startpageLoadingGif').modal('hide');
				}

			});
			response.error(function(data, status, headers, config) {
				$('#startpageLoadingGif').modal('hide');
				alert(data);
			});

		}

	}

});

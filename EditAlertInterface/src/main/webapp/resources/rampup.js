var myLoginApp = angular.module("rampup", [ "xeditable" ]);

myLoginApp
		.controller(
				"rampupController",
				function($scope, $http, $timeout, $filter) {

					$scope.rampupProcessList = [];
					$scope.rampupProcessNamesArray = [];
					$scope.rampupAdvertisementArray = [];
					$scope.rampupCountArray = [];

					$scope.clickRampupModel = {};
					$scope.addNewRecordModel = {};
					$scope.tempString = "";
					$scope.tempString_second = "";
					$scope.rampup_start_dateSelected = $filter('date')(
							new Date(), 'yyyy-MM-dd');
					$scope.dateSelected = $filter('date')(new Date(),
							'yyyy-MM-dd');
					$scope.responseData = "success";

					$scope.rampupStatus = [];

					// $scope.result = $filter('date')(new Date(), 'fullDate');

					$scope.initilizePathVariable = function(pathVariable) {

						$scope.pathVariable = pathVariable;
						$scope.rampupStatus = [];
						$scope.rampupStatus.push("Active");
						$scope.rampupStatus.push("InActive");
					}

					$scope.getAllrampupData = function(date) {
						
						$scope.rampupCountArray = [];

						console
								.log("in the rampup function $scope.pathVariable="
										+ $scope.pathVariable);

						$('#startpageLoadingGif').modal({
							backdrop : 'static',
							keyboard : false
						});

						var response = $http.get($scope.pathVariable
								+ "/rampup/?date=" + date);

						response
								.success(function(data, status, headers, config) {

									$('#startpageLoadingGif').modal('hide');

									console.log("Get all whitelables");

									// $scope.whitelables_tableHashmap = data;

									$scope.rampupProcessList = data;
									for (var i = 0; i < $scope.rampupProcessList.length; i++) {
										console
												.log("value of rampup="
														+ $scope.rampupProcessList[i].domain_type);

										$scope.localModel = $scope.rampupProcessList[i];
										console
												.log("$scope.localModel.is_rampup_live="
														+ $scope.localModel.is_rampup_live);

										if ($scope.localModel.ramp_up_type
												.toLowerCase().indexOf('fixed') != -1) {
											$scope.localModel.selected_ramp_type = "Fixed";
										} else if ($scope.localModel.ramp_up_type
												.toLowerCase().indexOf(
														'mailgun') != -1) {
											$scope.localModel.selected_ramp_type = "MailGun";

										}

										if ($scope.localModel.is_rampup_live == "1") {
											$scope.localModel.selected_rampup_status = "Active";

										} else if ($scope.localModel.is_rampup_live == "0") {
											$scope.localModel.selected_rampup_status = "InActive";
										}

										$scope.localModel.selected_user_domain_type = $scope.localModel.domain_type;

										// $scope.localModel.selected_user_domain_type
										// = "All";

										$scope.rampupProcessList[i] = $scope.localModel;

										$scope.clickRampupModel = $scope.localModel;

										// here we are removing duplicate record
										// of process

										if ($scope.tempString_second
												.indexOf($scope.localModel.processName) == -1) {
											$scope.rampupProcessNamesArray
													.push($scope.localModel.processName);

										}

										$scope.tempString_second = $scope.tempString_second
												+ "|"
												+ $scope.localModel.processName;

										// here we are removing duplicate record
										// of advertisement

										if ($scope.tempString
												.indexOf($scope.localModel.rampup_advertisement) == -1) {
											$scope.rampupAdvertisementArray
													.push($scope.localModel.rampup_advertisement);

										}
										$scope.tempString = $scope.tempString
												+ "|"
												+ $scope.localModel.rampup_advertisement;

										// console
										// .log("iterating map"
										// +
										// $scope.rampupProcessList[i].processName);

										// angular
										// .forEach(
										// $scope.rampupProcessList[i].domain_wise_map,
										// function(value, key) {
										// console.log(key);
										// console.log(value);
										//
										// });
									}
									// here we are inititalize new object
									$scope.rampupCountArray.push("25");
									$scope.rampupCountArray.push("50");
									$scope.rampupCountArray.push("100");
									$scope.initializeAddNewModel();
									// iteratinog hash map in angular

									// =============================//================
									// $scope.hashMapIterator_Function();

								});
						response.error(function(data, status, headers, config) {
							$('#startpageLoadingGif').modal('hide');
							alert(data);
						});

					}

					$scope.setClickRampupModel = function(rampupModel) {
						console.log("in the clickedrampupModel");
						$scope.clickRampupModel = rampupModel;
						console.log($scope.clickRampupModel.processName);

						$('#detail-dialog').modal({
							backdrop : 'static',
							keyboard : false
						});
					}

					$scope.addNewRecord = function() {

						$('#add-new-dialog').modal({
							backdrop : 'static',
							keyboard : false
						});
					}

					$scope.updateRampupInformation = function(rampupProcessList) {

						console.log("In the update information process");
						$scope.updatedRampupModelArray = [];

						for (var i = 0; i < rampupProcessList.length; i++) {

							if (rampupProcessList[i].isUpdateRequired == true) {
								rampupProcessList[i].ramp_up_type = rampupProcessList[i].selected_ramp_type;
								rampupProcessList[i].domain_type = rampupProcessList[i].selected_user_domain_type;

								$scope.updatedRampupModelArray
										.push(rampupProcessList[i]);

								// console
								// .log($scope.updatedRampupModelArray[0].domain_type);
								// console
								// .log($scope.updatedRampupModelArray[0].selected_ramp_type);

							}

						}

						$('#startpageLoadingGif').modal({
							backdrop : 'static',
							keyboard : false
						});

						var response = $http({
							method : 'POST',
							url : $scope.pathVariable + "/rampup-update/",
							data : $scope.updatedRampupModelArray
						});

						response
								.success(function(data, status, headers, config) {

									console.log("data=" + data);
									$('#startpageLoadingGif').modal('hide');
									$scope.responseData = "Successfully Updated!";
									console.log("$scope.responseData="
											+ $scope.responseData);
									$("#responsealert")
											.fadeTo(2000, 500)
											.slideUp(
													500,
													function() {
														$("#responsealert")
																.alert('close');
													});

									// console.log("Get all whitelables");

									// $scope.whitelables_tableHashmap = data;

									// =============================//================
									// $scope.hashMapIterator_Function();

								});
						response.error(function(data, status, headers, config) {
							$('#startpageLoadingGif').modal('hide');
							alert(data);
						});

					}

					$scope.isUpdateRequired = function(rampupModel) {

						console.log("in update required method for"
								+ rampupModel.processName);
						rampupModel.isUpdateRequired = true;

					}

					$scope.addNewRecordOfRampup = function() {

						console.log("in the add new rampup object");
						$scope.addNewRecordModel.date = $scope.rampup_start_dateSelected;
						$scope.addNewRecordModel.selected_ramp_type = $scope.addNewRecordModel.ramp_up_type;
						$scope.addNewRecordModel.is_rampup_live = "1";
						$scope.addNewRecordModel.selected_rampup_status = "Active";

						$('#startpageLoadingGif').modal({
							backdrop : 'static',
							keyboard : false
						});

						var response = $http({
							method : 'POST',
							url : $scope.pathVariable + "/addnew-rampup/",
							data : $scope.addNewRecordModel
						});

						response
								.success(function(data, status, headers, config) {

									$('#startpageLoadingGif').modal('hide');
									

									console.log("$scope.addNewRecordModel.id "+$scope.addNewRecordModel.id);
									if ($scope.dateSelected == $scope.rampup_start_dateSelected) {
										$scope.addNewRecordModel.id=data;
										$scope.rampupProcessList
												.push($scope.addNewRecordModel);
									}

									$scope.addNewRecordModel = null;
									$scope.addNewRecordModel = {};
									$scope.initializeAddNewModel();

									$scope.responseData = "Successfully Added!";
									console.log("$scope.responseData="
											+ $scope.responseData);

									$("#responsealert")
											.fadeTo(2000, 500)
											.slideUp(
													500,
													function() {
														$("#responsealert")
																.alert('close');
													});

									// console.log("Get all whitelables");

									// $scope.whitelables_tableHashmap = data;

									// =============================//================
									// $scope.hashMapIterator_Function();

								});
						response.error(function(data, status, headers, config) {
							$('#startpageLoadingGif').modal('hide');
							alert(data);
						});

					}

					$scope.initializeAddNewModel = function() {
						$scope.addNewRecordModel.ramp_up_type = "Fixed";
						$scope.addNewRecordModel.rampup_types = $scope.rampupProcessList[0].rampup_types;
						$scope.addNewRecordModel.user_domain_type = $scope.rampupProcessList[0].user_domain_type;
						$scope.addNewRecordModel.selected_user_domain_type = "All";
						$scope.addNewRecordModel.processName = $scope.rampupProcessList[0].processName;
						$scope.addNewRecordModel.ramp_users_count = "25";
						$scope.addNewRecordModel.whitelable_table_map = $scope.rampupProcessList[0].whitelable_table_map;
						$scope.addNewRecordModel.new_rampup_advertisement = $scope.rampupProcessList[0].new_rampup_advertisement;
						$scope.addNewRecordModel.rampup_advertisement = $scope.addNewRecordModel.new_rampup_advertisement[0];

					}

				});

$(function() {

	$("#datepicker_startdate").datepicker({
		dateFormat : 'yy-mm-dd'
	});
	$("#datepicker_startdate").datepicker("setDate", 'today');

	$("#datepicker_enddate").datepicker({
		dateFormat : 'yy-mm-dd'
	});
});
$(function() {

	$("#rampup_start_dateSelected").datepicker({
		dateFormat : 'yy-mm-dd'
	});
	$("#rampup_start_dateSelected").datepicker("setDate", 'today');

	$("#rampup_start_dateSelected").datepicker({
		dateFormat : 'yy-mm-dd'
	});
});

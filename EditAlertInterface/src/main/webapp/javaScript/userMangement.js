var pivotApp = angular.module("userMangementApp", []);

pivotApp.controller("userMangementController",function($scope, $http, $timeout, $filter, $interval, $compile) {

					$scope.usersList = [];

					$scope.initilizePathVariable = function(pathVariable) {
						$scope.pathVariable = pathVariable;
					}

					
					$scope.getUsersDetials = function() {
						console.log("in method");
						$http({
							method : 'GET',
							url : $scope.pathVariable + '/api/get_users'
						}).then(function successCallback(response) {

							$scope.usersList = response.data;
							console.log($scope.usersList[0].role);
							$scope.createUserTable($scope.usersList);

						}, function errorCallback(response) {
							$scope.isDisabled = false;
							$scope.showLoadingImage = false;
						});
					}

					/*-------------------------------------------------------------------------------------
					 * 
					 *  User Management Table
					 * 
					 ---------------------------------------------------------------------------------------*/
					$scope.createUserTable = function(usersList) {
						document.getElementById('loader').style.display = 'inline';
						if ($("#usersTable").length > 0) {
							document.getElementById('save').style.display = 'none';
							document.getElementById('cancel').style.display = 'none';
							document.getElementById('adduser').style.display = 'inline';
							document.getElementById('table_content').removeChild(document.getElementById('usersTable'));
						}
						var table = '<table id="usersTable" class="table table-hover">';
						var role = "";
						for (var i = 0; i < usersList.length; i++) {

							if (usersList[i].role == ("jsource"))
								role = "(Job Source)";
							else
								role = "(Job Seeker)";

							table += '<td>' + usersList[i].email + '</td>';
							table += '<td>' + role + '</td>';
							table += '<td><span id="deleteLink_'
									+ usersList[i].email
									+ '" ng-click="deleteUser();" class="deleteLink">Delete</span></td>';
							
							if (usersList[i].click_role == ("2"))
								table += '<td><select id="click_'+ usersList[i].email+ '"><option value="1">All</option><option selected value="2">Unique</option><option value="3">Gross</option></select></td>';
							else if (usersList[i].click_role == ("3"))
								table += '<td><select id="click_'+ usersList[i].email+ '"><option value="1">All</option><option value="2">Unique</option><option selected value="3">Gross</option></select></td>';
							else 
								table += '<td><select id="click_'+ usersList[i].email+ '"><option value="1">All</option><option value="2">Unique</option><option value="3">Gross</option></select></td>';
						
							table += '<td><button id="forward_'+ usersList[i].email+ '_'+ usersList[i].role+'_'+ usersList[i].privilage
									+ '" ng-click="forwardRequest();" style="border-radius: 5px; background-color: #333333; height: 25px; line-height: 25px; color: #ffffff;"> > </button></td>';
							table += '</tr>';
						}
						table += '</table>';

						// angular.element(document.getElementById('table_content'))
						// .append($compile("<div id='table_div'
						// class='table-responsive'></div>")($scope));

						angular.element(
								document.getElementById('table_content'))
								.append($compile(table)($scope));
						document.getElementById('loader').style.display="none";
					}

					/***********************************************************
					 * --------------------------------------------------------
					 * Delete User
					 * --------------------------------------------------------
					 **********************************************************/

					$scope.deleteUser = function() {
						console.log("delete");
						var btnId = event.target.id;
						var r = confirm("Do You want to delete "+btnId.split("_")[1]+"?");
						if (r == true) {
						
						var model={};
						model.email=btnId.split("_")[1];
						
						$http( { method : 'POST',
								data : model,
								url : $scope.pathVariable +'/api/delete_update_user?delupdate=delete'
						  }).then(function successCallback(response) {
							  document.getElementById("message").style.color = "red";
							  document.getElementById("message").innerHTML = "User Deleted";
							  $scope.getUsersDetials();
							  document.getElementById('loader').style.display = 'inline';
							  
						   }, function errorCallback(response) {
						  $scope.isDisabled = false; $scope.showLoadingImage =false; });
						 
						document.getElementById('table_content').removeChild(document.getElementById('usersTable'));
						}
					}

					/***********************************************************
					 * --------------------------------------------------------
					 * Forward Request
					 * --------------------------------------------------------
					 **********************************************************/

					$scope.forwardRequest = function() {
						
						document.getElementById('loader').style.display = 'inline';
						document.getElementById("message").innerHTML = '';
						document.getElementById('adduser').style.display = 'none';
						
						$scope.sourceOrProviderList = [];
						var btnId = event.target.id;
						$scope.user = btnId.split("_")[1];
						$scope.click_role=document.getElementById('click_'+$scope.user).value;
						var role = btnId.split("_")[2];
						var privilage = btnId.split("_")[3];
						var api = "";
						var alphabet = "";
						document.getElementById('table_content').removeChild(document.getElementById('usersTable'));
						if (role == "jsource")
							api = "sources";
						else
							api = "providers";
						$http(
								{
									method : 'GET',
									url : $scope.pathVariable
											+ '/api/getSourceProviders?sourceOrProvider='
											+ api
								})
								.then(
										function successCallback(response) {

											$scope.sourceOrProviderList = response.data;
											var flag = 0;
											var table = '<table id="usersTable" class="table table-hover">';
											table += '<tr><td style="font-weight: bold;border:0;"><input type="checkbox" id="selectAll" ng-click="selectAll(this);';
											if(privilage=="all")
												table +=' checked ';	
											table += '">&nbsp;&nbsp;&nbsp;Select All';
											
											
											for (var i = 0; i < $scope.sourceOrProviderList.length; i++) {

												var entry = $scope.sourceOrProviderList[i];

												if (entry.charAt(0).toUpperCase() != alphabet) {
													alphabet = entry.charAt(0)
															.toUpperCase();
													var saveButton = "";
													if (flag == 0) {
														flag = 1;
														saveButton = '<td></td>';
													}
													table += '<tr style="background-color:#D8D8D8;"><td>'
															+ alphabet
															+ '</td><tr>';
												}
												if(privilage.includes(entry) || privilage=="all")
													table += '<tr><td><input type="checkbox" checked name="check" value="'+ entry+ '" id="check_'+ entry+ '">&nbsp;&nbsp;&nbsp;';
												else
													table += '<tr><td><input type="checkbox" name="check" value="'+ entry+ '" id="check_'+ entry+ '">&nbsp;&nbsp;&nbsp;';
												table += entry + '</td></tr>';
											}
											table += '</table>';

											angular.element(document.getElementById('table_content')).append($compile(table)($scope));
											document.getElementById('loader').style.display="none";
											document.getElementById('save').style.display = 'inline';
											document.getElementById('cancel').style.display = 'inline';
											
										}, function errorCallback(response) {
											$scope.isDisabled = false;
											$scope.showLoadingImage = false;
										});

					}

					/***********************************************************
					 * --------------------------------------------------------
					 * ADD USERs
					 * --------------------------------------------------------
					 **********************************************************/

					$scope.addUser = function() {

						var mUserDataModel = {};
						var email = document.getElementById("email");
						var password = document.getElementById("password");
						if (email.value == '') {
							email.style.borderColor = "red";
						} else if (password.value =='' || password.value != document.getElementById("confirm_Password").value) {
							password.value = '';
							document.getElementById("confirm_Password").value = '';
							password.style.borderColor = "red";
							document.getElementById("confirm_Password").style.borderColor = "red";
						} else if (document.getElementById("role").value == 'Select Role') {
							document.getElementById("role").style.borderColor = "red";
						} else {
							mUserDataModel.email = email.value;
							mUserDataModel.password = password.value;
							mUserDataModel.role = document.getElementById("role").value;
							$(imple).modal('hide');
							$http({
								method : 'POST',
								data : mUserDataModel,
								url : $scope.pathVariable + '/api/add_user'
							})
									.then(
											function successCallback(response) {

												var res = response.data;
												console.log(res.status);
												if (res.status == true) {
													document.getElementById("message").innerHTML = res.message;
													$scope.getUsersDetials();
												} else {
													document.getElementById("message").style.color = "red";
													document.getElementById("message").innerHTML = res.message;
												}
												// g.detach().trigger("closed.bs.alert").remove()
												// $('imple').modal('toggle');

											},
											function errorCallback(response) {
												$scope.isDisabled = false;
												$scope.showLoadingImage = false;
												alert(response.data);

											});
						}
					}

					/***********************************************************
					 * Select ALL
					 * 
					 **********************************************************/

					$scope.selectAll = function(source) {

						$("#selectAll").change(function () {

						    $("input:checkbox").prop('checked', $(this).prop("checked"));
						});
						
//						$('#selectAll').click(function(event) {
//							if (this.checked) {
//								console.log("if");
//								$(':checkbox').each(function() {
//									this.checked = true;
//								});
//							} else {
//								console.log("else");
//								$(':checkbox').each(function() {
//									this.checked = false;
//								});
//
//							}
//						});
					}
					$scope.updatePrivilages = function(source) {

						console.log("save");
						var yourArray = [];

						var checkboxes = document.querySelectorAll('input[name="check"]:checked');
						
						Array.prototype.forEach.call(checkboxes, function(el) {
							yourArray.push(el.value);
						});

						var privilages = "";
						if ($scope.sourceOrProviderList.length == yourArray.length) {
							privilages = "all";
						} else {
							for (var i = 0; i < yourArray.length; i++) {
								if(i==0)
									privilages=yourArray[i];
								else
									privilages=privilages+","+yourArray[i];
							}
						}
						
						console.log(privilages);
						var model={};
						model.privilage=privilages;
						model.email=$scope.user;
						model.click_role=$scope.click_role;
						  $http( { method : 'POST',
								data : model,
								url : $scope.pathVariable +'/api/delete_update_user'
						  }).then(function successCallback(response) {
							  document.getElementById("message").innerHTML = "Update user Successfully";
							  document.getElementById('loader').style.display = 'inline';
							  $scope.getUsersDetials();
							  
						   }, function errorCallback(response) {
						  $scope.isDisabled = false; $scope.showLoadingImage =false;  });
						 
					}
				});

var pivotApp = angular.module("pivotApp",[]);

pivotApp.controller("pivotAppController", function($scope, $http, $timeout, $filter, $interval, $compile) {
	
	$scope.initilizePathVariable = function(pathVariable) {
		console.log("startjal");
		$scope.pathVariable = pathVariable;
	}
	
	$scope.exportPivot= function() {
		console.log("start");
		$http(
				{
					method : 'GET',
					url : $scope.pathVariable+'/api/exportPivot'
				}).then(function successCallback(response) {
					
			alert(response.data);

		}, function errorCallback(response) {
			alert(response.data);
		});
	}  
	
});
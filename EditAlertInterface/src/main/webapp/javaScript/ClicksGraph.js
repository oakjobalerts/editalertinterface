var pivotApp = angular.module("clicksApp", [ "highcharts-ng" ]);

pivotApp.controller("clicksController", function($scope, $http, $timeout, $filter, $interval, $compile) {

	$scope.initilizePathVariable = function(pathVariable) {
		$scope.pathVariable = pathVariable;
	}

	$scope.setDefault = function(month, year) {
		document.getElementById('month').value = month;
		document.getElementById('year').value = year;
	}

	$scope.colorArray=[
	    "#FF0080", "#FF0040", "#0101DF", "#084B8A", "#01DF01", "#FFBF00", "#FF0000", "#FA5858", 
	    "#FFFF00", "#5FB404", "#4B8A08", "#0B610B", "#01DFD7", "#98D058", "#5F04B4", "#9A2EFE", 
	    "#610B4B", "#FE2E64", "#FF00BF", "#FF4000", "#B43104",
        
	    "#610B21", "#610B5E", "#FE2E64", "#E2A9F3", "#81F79F", "#045FB4", "#8A0808", "#8181F7", 
	    "#61210B", "#A9A9F5", "#886A08", "#5FB404", "#088A68", "#F781F3", "#0B4C5F", "#380B61", 
	    "#610B4B", "#610B38", "#610B21", "#DF013A", "#F7FE2E", "#F78181", "#82FA58", "#012C58",
        "#5B4534", "#FDE8DC", "#404E55", "#0089A3", "#CB7E98", "#A4E804", "#324E72", "#6A3A4C",
        "#83AB58", "#001C1E", "#D1F7CE", "#004B28", "#C8D0F6", "#A3A489", "#806C66", "#222800",
        "#BF5650", "#E83000", "#66796D", "#DA007C", "#FF1A59", "#8ADBB4", "#1E0200", "#5B4E51",
        "#C895C5", "#320033", "#FF6832", "#66E1D3", "#CFCDAC", "#D0AC94", "#7ED379", 
        
	    "#671190", "#6B3A64", "#F5E1FF", "#FFA0F2", "#CCAA35", "#374527", "#8BB400", "#797868",
        "#C6005A", "#3B000A", "#C86240", "#29607C", "#402334", "#7D5A44", "#CCB87C", "#B88183",
        "#AA5199", "#B5D6C3", "#A38469", "#9F94F0", "#A74571", "#B894A6", "#71BB8C", "#00B433",
        "#789EC9", "#6D80BA", "#953F00", "#5EFF03", "#E4FFFC", "#1BE177", "#BCB1E5", "#76912F",
        "#003109", "#0060CD", "#D20096", "#895563", "#29201D", "#5B3213", "#A76F42", "#89412E",
        "#1A3A2A", "#494B5A", "#A88C85", "#F4ABAA", "#A3F3AB", "#00C6C8", "#EA8B66", "#958A9F",
        "#BDC9D2", "#9FA064", "#BE4700", "#658188", "#83A485", "#453C23", "#47675D", "#3A3F00",
        "#061203", "#DFFB71", "#868E7E", "#98D058", "#6C8F7D", "#D7BFC2", "#3C3E6E", "#D83D66",
	    
	   
        "#7A7BFF", "#D68E01", "#353339", "#78AFA1", "#FEB2C6", "#75797C", "#837393", "#943A4D",
        "#B5F4FF", "#D2DCD5", "#9556BD", "#6A714A", "#001325", "#02525F", "#0AA3F7", "#E98176",
        "#DBD5DD", "#5EBCD1", "#3D4F44", "#7E6405", "#02684E", "#962B75", "#8D8546", "#9695C5",
        "#E773CE", "#D86A78", "#3E89BE", "#CA834E", "#518A87", "#5B113C", "#55813B", "#E704C4",
        "#00005F", "#A97399", "#4B8160", "#59738A", "#FF5DA7", "#F7C9BF", "#643127", "#513A01",
        "#6B94AA", "#51A058", "#A45B02", "#1D1702", "#E20027", "#E7AB63", "#4C6001", "#9C6966",
        "#64547B", "#97979E", "#006A66", "#391406", "#F4D749", "#0045D2", "#006C31", "#DDB6D0",
        "#7C6571", "#9FB2A4", "#00D891", "#15A08A", "#BC65E9", "#FFFFFE", "#C6DC99", "#203B3C",

        
        "#2F5D9B", "#6C5E46", "#D25B88", "#5B656C", "#00B57F", "#545C46", "#866097", "#365D25",
        "#252F99", "#00CCFF", "#674E60", "#FC009C", "#92896B",
        "#003366","#00CC99","#660033","#FF99FF","#FF0066","#CC6600","#336600","#0099CC","#848484","#DF013A",
        "#D358F7","#FF0000","#00FFFF","#E1F5A9","#C2E0D1","#59955C","#5F4C0B","#E3D6AA","#00CC99","#00CC99",
        "#01DFA5","#666680","#BDBDBD","#FA8258","#DF0174","#8A0886","#210B61","#01DF01","#FACC2E","#088A68",
        "#666680","#99C2FF","#DBB8FF","#FF99CC","#B2246B","#CCCC52","#CCEBCC","#003D00","#8F8FB2","#8AE6B8",
        "#FFCCCC","#B28F8F","#665252","#D17519","#A35200","#EBC299","#CCFF66","#B8E65C","#754719","#7575A3",
        "#CC6600","#33FF33","#33CCFF","#993333","#666666","#CCCCCC","#66FF99","#66FF99","#003366","#00CC99",
        "#660033","#FF99FF","#FF0066","#CC6600","#336600","#0099CC","#848484","#DF013A",
        "#003366","#00CC99","#660033","#FF99FF","#FF0066","#CC6600","#336600","#0099CC","#848484","#DF013A",
        "#D358F7","#FF0000","#00FFFF","#E1F5A9","#C2E0D1","#59955C","#5F4C0B","#E3D6AA","#00CC99","#00CC99",
        "#01DFA5","#666680","#BDBDBD","#FA8258","#DF0174","#8A0886","#210B61","#01DF01","#FACC2E","#088A68",
        "#666680","#99C2FF","#DBB8FF","#FF99CC","#B2246B","#CCCC52","#CCEBCC","#003D00","#8F8FB2","#8AE6B8",
        "#FFCCCC","#B28F8F","#665252","#D17519","#A35200","#EBC299","#CCFF66","#B8E65C","#754719","#7575A3",
        "#CC6600","#33FF33","#33CCFF","#993333","#666666","#CCCCCC","#66FF99","#66FF99","#003366","#00CC99",
        "#660033","#FF99FF","#FF0066","#CC6600","#336600","#0099CC","#848484","#DF013A"];
	
	$scope.uniqueGlobal = [];
	$scope.grossGlobal = [];

	$scope.drawClicksGraph = function(sourceOrProvider, role, privilage, click_role) {

		if (document.getElementById('statusDropdown').value == 'Monthly') {
			$scope.getMonthlyClicks(sourceOrProvider, role, privilage, click_role);
		} else {
			document.getElementById('loader').style.display = "inline";
			$scope.sourceOrProvider = sourceOrProvider;
			var folder = document.getElementById('month').value + '_' + document.getElementById('year').value;

			console.log("folder:" + folder);
			$http({
				method : 'GET',
				url : $scope.pathVariable + '/api/drawClicksGraph?sourceOrProvider=' + $scope.sourceOrProvider + '&role=' + role + '&privilage=' + privilage + '&folderName=' + folder
			}).then(function successCallback(response) {

				var highchartModel = response.data;

				clicksDataList = highchartModel.clicksData;
				$scope.categories = highchartModel.categories;
				$scope.uniqueClicksData = [];
				$scope.grossClicksData = [];

				var len = clicksDataList.length;

				for (var i = 0; i < len; i++) {
					// for(var i=0;i<80;i++){

					var seriesObjUnique = {};
					var seriesObjGross = {};
					var globalObjUnique = {};
					var globalObjGross = {};

					seriesObjUnique.name = clicksDataList[i].name;
					seriesObjUnique.data = clicksDataList[i].unique;
					seriesObjUnique.color = $scope.colorArray[i];
					$scope.uniqueClicksData.push(seriesObjUnique);

					seriesObjGross.name = clicksDataList[i].name;
					seriesObjGross.data = clicksDataList[i].gross;
					seriesObjGross.color = $scope.colorArray[i];
					$scope.grossClicksData.push(seriesObjGross);

					var innerlen = clicksDataList[i].uniqueTotal.length;
					console.log("innerlen:" + innerlen);
					if (innerlen > 0) {
						globalObjUnique.name = clicksDataList[i].name;
						globalObjUnique.data = clicksDataList[i].uniqueTotal;
						globalObjUnique.color = $scope.colorArray[i];
						$scope.uniqueGlobal.push(globalObjUnique);

						globalObjGross.name = clicksDataList[i].name;
						globalObjGross.data = clicksDataList[i].grossTotal;
						globalObjGross.color = $scope.colorArray[i];
						$scope.grossGlobal.push(globalObjGross);
					}
				}

				if ($("#high_chart").length > 0) {
					document.getElementById('container').removeChild(document.getElementById('high_chart'));
				} else {
					if (click_role == 1) {
						angular.element(document.getElementById('container')).append($compile("<div style='margin-bottom:10px;'>" + "<button id='unique' style='background: #192e6c;color:#fff; margin-left:5px;width:60px;border-radius: 5px;padding: 5px;font-size: 14px;' ng-click='createGraph(uniqueClicksData,uniqueGlobal);' >Unique</button>" + "<button id='gross' style='background: #E9ECF0;color:#000;margin-left:5px;width:60px;border-radius: 5px;padding: 5px;font-size: 14px;' ng-click='createGraph(grossClicksData,grossGlobal);' >Gross</button></div>")($scope));
					} else if (click_role == 2) {
						angular.element(document.getElementById('container')).append($compile("<div style='margin-bottom:10px;'>" + "<button id='unique' style=background: #192e6c;color:#fff;margin-left:5px;width:60px;border-radius: 5px;padding: 5px;font-size: 14px;' ng-click='createGraph(uniqueClicksData,uniqueGlobal);' >Unique</button>")($scope));
					}
					if (click_role == 3) {
						angular.element(document.getElementById('container')).append($compile("<div style='margin-bottom:10px;'>" + "<button id='gross' style='background: #192e6c;color:#fff;margin-left:5px;width:60px;border-radius: 5px;padding: 5px;font-size: 14px;' ng-click='createGraph(grossClicksData,grossGlobal);' >Gross</button></div>")($scope));
					}
				}

				angular.element(document.getElementById('container')).append($compile("<div style='height:500px;' id='high_chart'></div>")($scope));

				angular.element(document.getElementById('contentTable')).append($compile("<div id='table_div'></div>")($scope));
				angular.element(document.getElementById('contentTable')).append($compile("<div id='global_table_div' class='table-responsive' style='margin-top:40px;'></div>")($scope));

				if (click_role == 3)
					$scope.createGraph($scope.grossClicksData, $scope.grossGlobal);
				else
					$scope.createGraph($scope.uniqueClicksData, $scope.uniqueGlobal);

			}, function errorCallback(response) {
				$scope.isDisabled = false;
				$scope.showLoadingImage = false;
			});
		}
	}

	$scope.getMonthlyClicks = function(sourceOrProvider, role, privilage, click_role) {
		console.log("for monthly clicks");
		document.getElementById('loader').style.display = "inline";
		var year = document.getElementById('year').value;

		$http({
			method : 'GET',
			url : $scope.pathVariable + '/api/getMonthlyClicks?year=' + year + '&sourceOrProvider=' + sourceOrProvider + '&role=' + role + '&privilage=' + privilage
		}).then(function successCallback(response) {

			var highchartModel = response.data;

			clicksDataList = highchartModel.clicksData;
			$scope.categories = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 ];
			$scope.uniqueClicksData = [];
			$scope.grossClicksData = [];

			var len = clicksDataList.length;

			for (var i = 0; i < len; i++) {
				// for(var i=0;i<80;i++){

				var seriesObjUnique = {};
				var seriesObjGross = {};
				var globalObjUnique = {};
				var globalObjGross = {};

				seriesObjUnique.name = clicksDataList[i].name;
				seriesObjUnique.data = clicksDataList[i].unique;
				seriesObjUnique.color = $scope.colorArray[i];
				$scope.uniqueClicksData.push(seriesObjUnique);

				seriesObjGross.name = clicksDataList[i].name;
				seriesObjGross.data = clicksDataList[i].gross;
				seriesObjGross.color = $scope.colorArray[i];
				$scope.grossClicksData.push(seriesObjGross);

			}

			if ($("#high_chart").length > 0) {
				document.getElementById('container').removeChild(document.getElementById('high_chart'));
			} else {
				if (click_role == 1) {
					angular.element(document.getElementById('container')).append($compile("<div style='margin-bottom:10px;'>" + "<button id='unique' style='background: #192e6c;color:#fff; margin-left:5px;width:60px;border-radius: 5px;padding: 5px;font-size: 14px;' ng-click='createGraph(uniqueClicksData,uniqueGlobal);' >Unique</button>" + "<button id='gross' style='background: #E9ECF0;color:#000;margin-left:5px;width:60px;border-radius: 5px;padding: 5px;font-size: 14px;' ng-click='createGraph(grossClicksData,grossGlobal);' >Gross</button></div>")($scope));
				} else if (click_role == 2) {
					angular.element(document.getElementById('container')).append($compile("<div style='margin-bottom:10px;'>" + "<button id='unique' style=background: #192e6c;color:#fff;margin-left:5px;width:60px;border-radius: 5px;padding: 5px;font-size: 14px;' ng-click='createGraph(uniqueClicksData,uniqueGlobal);' >Unique</button>")($scope));
				}
				if (click_role == 3) {
					angular.element(document.getElementById('container')).append($compile("<div style='margin-bottom:10px;'>" + "<button id='gross' style='background: #192e6c;color:#fff;margin-left:5px;width:60px;border-radius: 5px;padding: 5px;font-size: 14px;' ng-click='createGraph(grossClicksData,grossGlobal);' >Gross</button></div>")($scope));
				}
			}

			angular.element(document.getElementById('container')).append($compile("<div style='height:500px;' id='high_chart'></div>")($scope));

			angular.element(document.getElementById('contentTable')).append($compile("<div id='table_div' class=''></div>")($scope));
			angular.element(document.getElementById('contentTable')).append($compile("<div id='global_table_div' class='table-responsive' style='margin-top:40px;'></div>")($scope));

			if (click_role == 3)
				$scope.createGraph($scope.grossClicksData, $scope.grossGlobal);
			else
				$scope.createGraph($scope.uniqueClicksData, $scope.uniqueGlobal);

		}, function errorCallback(response) {
			$scope.isDisabled = false;
			$scope.showLoadingImage = false;
		});
	}

	$scope.createDailyClicksTable = function(clicksData) {

		var table = '<table id="dailyTable" border="1" style="border:1px solid black;min-width:70%;margin:0 auto;"><tr style="border:1px solid black; background:gray; color:#fff"><th  style="text-align: center;width:32px;">Daily Clicks Report </th>';

		for (var i = 0; i < $scope.categories.length; i++) {
			table += '<th style="text-align: center;width:32px;">' + $scope.categories[i] + '</th>';
		}
		table += '<th style="text-align: center;width:32px;">Total</th></tr>';

		var belowTotal = "";
		var total = [];
		var horizotalTotal = 0;
		for (var i = 0; i < clicksData.length; i++) {
			belowTotal += '<tr><td style="text-align: center;padding: 1px;">' + clicksData[i].name + '</td>';
			var clicks = clicksData[i].data;
			horizotalTotal = 0;
			for (var j = 0; j < clicks.length; j++) {
				belowTotal += '<td style="text-align: center;padding: 1px;">' + clicks[j] + '</td>';

				if (total.length <= j)
					total[j] = clicks[j];
				else
					total[j] += clicks[j];

				horizotalTotal += clicks[j];

			}

			belowTotal += '<td style="text-align: center;background:#ccc;padding: 1px;">' + horizotalTotal + '</td></tr>';
		}
		belowTotal += '</table>';

		table += '<tr style="background:#ccc;"><td style="text-align: center;">Total</td>';
		var totalOftotal = 0;
		for (var j = 0; j < total.length; j++) {
			totalOftotal += total[j];
			table += '<td style="text-align: center;padding: 2px;">' + total[j] + '</td>';
		}
		table += '<td style="text-align: center;padding: 2px;">' + totalOftotal + '</td></tr>';
		table += belowTotal;

		if ($("#dailyTable").length > 0) {
			document.getElementById('table_div').removeChild(document.getElementById('dailyTable'));
		}

		angular.element(document.getElementById('table_div')).append($compile(table)($scope));
		document.getElementById('loader').style.display = "none";
	}

	$scope.createGlobalTable = function(clicksData) {

		var table = '<table id="globalTable" border="1" style="border:1px solid black;min-width:70%;margin:0 auto;"><tr style="border:1px solid black; background:gray; color:#fff"><th  style="text-align: center;width:32px;">Total Clicks Report</th>';

		table += '<th style="text-align: center;width:32px;">Today</th>';
		table += '<th style="text-align: center;width:32px;">Yesterday</th>';
		table += '<th style="text-align: center;width:32px;">Last Seven Days</th>';
		table += '<th style="text-align: center;width:32px;">This Month</th>';
		table += '<th style="text-align: center;width:32px;">Last Month</th>';
		table += '</tr>';

		var belowTotal = "";
		var total = [];
		for (var i = 0; i < clicksData.length; i++) {
			belowTotal += '<tr><td style="text-align: center;padding: 2px;">' + clicksData[i].name + '</td>';
			var clicks = clicksData[i].data;

			for (var j = 0; j < clicks.length; j++) {
				belowTotal += '<td style="text-align: center;padding: 2px;">' + clicks[j] + '</td>';

				if (total.length <= j)
					total[j] = clicks[j];
				else
					total[j] += clicks[j];

			}

			belowTotal += '</tr>';
		}
		belowTotal += '</table>';

		table += '<tr style="background:#ccc;"><td style="text-align: center;">Total</td>';
		for (var j = 0; j < total.length; j++) {
			table += '<td style="text-align: center;padding: 2px;">' + total[j] + '</td>';
		}
		table += '</tr>';
		table += belowTotal;

		if ($("#globalTable").length > 0) {
			document.getElementById('global_table_div').removeChild(document.getElementById('globalTable'));
		}

		angular.element(document.getElementById('global_table_div')).append($compile(table)($scope));
		document.getElementById('loader').style.display = "none";
	}
	$scope.createGraph = function(clicksData, globalData) {

		if (event.target.id == 'unique') {

			document.getElementById('gross').style.background = '#E9ECF0';
			document.getElementById('gross').style.color = '#000';
			document.getElementById('unique').style.background = '#192e6c';
			document.getElementById('unique').style.color = '#fff';
		} else if (event.target.id == 'gross') {
			document.getElementById('unique').style.background = '#E9ECF0';
			document.getElementById('unique').style.color = '#000';
			document.getElementById('gross').style.background = '#192e6c';
			document.getElementById('gross').style.color = '#fff';
		}

		var json = {};

		json.chart = {
			type : 'column'
		};
		json.chartArea = {
			height : '50%',
		};
		json.title = {
			text : ''
		};
		json.xAxis = {
			categories : $scope.categories
		};
		json.yAxis = {
			min : 0,
			title : {
				text : 'Total Clicks Day To Day'
			},
			stackLabels : {
				enabled : true,
				style : {
					fontWeight : 'bold',
					borderWidth : 0,
					color : (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
				}
			}
		};
		json.legend = {
			align : 'left',
			x : 10,
			verticalAlign : 'bottom',
			y : 0,
			floating : false,
			backgroundColor : (Highcharts.theme && Highcharts.theme.background2) || 'white',
			borderColor : '#CCC',
			borderWidth : 1,
			shadow : false
		};
		json.tooltip = {
			headerFormat : '<b>{point.x}</b><br/>',
			pointFormat : '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
		};
		json.plotOptions = {
			column : {
				borderWidth:0,
				stacking : 'normal',
				dataLabels : {
					enabled : false,
					color : (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
				}
			}
		};
		json.series = clicksData;

		Highcharts.chart('high_chart', json);

		$scope.createDailyClicksTable(clicksData);
		$scope.createGlobalTable(globalData);

		// $('#container').highcharts(json);
	}

});

$(function() {
	$("#datepicker_startdate").datepicker({
		dateFormat : 'yy-mm-dd'
	});

	$("#datepicker_enddate").datepicker({
		dateFormat : 'yy-mm-dd'
	});
});

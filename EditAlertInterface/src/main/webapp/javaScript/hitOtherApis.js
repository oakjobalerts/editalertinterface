
var otherApiApp = angular.module("otherApiApp", ["highcharts-ng"]);

otherApiApp.controller("otherApiAppController", function($scope, $http, $timeout,
		$filter, $interval, $compile) {

	$scope.initilizePathVariable = function(pathVariable) {
		$scope.pathVariable = pathVariable;
	}

	$scope.drawClicksGraph = function() {
		console.log("done");
		var todate=document.getElementById('to_date').value;
		var fromDate=document.getElementById('from_date').value;
		
		console.log(todate);
		console.log(fromDate);
		
		document.getElementById('loader').style.display="inline";

				$http({
							method : 'GET',
							url : $scope.pathVariable
									+ '/api/getHourlyClicks?from='+fromDate+'&to='+todate
						}).then(function successCallback(response) {
							
							var seriesdata=[];
							seriesdata=response.data;
							
							var category=[];
							for(var i=1;i<=24;i++){
								category.push(i);
							}
							document.getElementById('loader').style.display="none";
							if ($("#high_chart").length > 0) {
								document.getElementById('container').removeChild(document.getElementById('high_chart'));
							}
							angular.element(document.getElementById('container')).append($compile("<div style='height:500px;' id='high_chart'></div>")($scope));
								$scope.createLineGraph(seriesdata,category);
		
				}, function errorCallback(response) {
					$scope.isDisabled = false;
					$scope.showLoadingImage = false;
				});
		 }

	$scope.createLineGraph = function(seriesdata,category) {
		
		var json = {};
		
		json.title= {
		            text: 'Hour Clicks',
		            x: -20 //center
		        	};
		
		json.subtitle= {
		            text: '',
		            x: -20
					};
		
		json.xAxis= {
		            categories: category
		        	};
		json.yAxis= {
		            title: {
		                text: ''
		            },
		            plotLines: [{
		                value: 0,
		                width: 1,
		                color: '#808080'
		            }]
		        };
		json.tooltip= {
		            valueSuffix: ''
		        	};
		json.legend= {
		            layout: 'vertical',
		            align: 'right',
		            verticalAlign: 'middle',
		            borderWidth: 0
		        	};
		json.series= seriesdata;
		
		Highcharts.chart('high_chart', json);
	}
	
});
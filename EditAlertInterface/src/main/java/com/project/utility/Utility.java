package com.project.utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.project.model.DomainWiseUserStats;
import com.project.model.MyMainModel;
import com.project.model.RampupModel;

public class Utility {

	// public static Map<String, List<String>> whitelables_tables_map_list = new
	// HashMap<String, List<String>>();

	public static MyMainModel oneTime_MainModel = new MyMainModel();
	public static SimpleDateFormat dateFormat = new SimpleDateFormat(
			"yyyy-MM-dd");
	// public static String filePath =
	// "/home/signity/EditAlertProject/myfile.json";

	// live
	public static String filePath = "/var/nfs-93/redirect/mis_logs/EditAlertProject/myfile.json";
	public static String domain_wise_file = "/var/nfs-93/redirect/mis_logs/DomainWiseUsers"
			+ File.separator + "domain_wise_user_";

	// ==================================for rampup
	// process===========================

	// local file path
	// public static String domain_wise_file = "/home/signity/EditAlertProject"
	// + File.separator + "domain_wise_user_";

	// here we are reading the domain wise stats

	// live file path
	// public static String domain_wise_file = "/home/signity/EditAlertProject";

	// String path = Utility.domain_wise_file + "" + File.separator
	// + "domain_wise_user_" + Utility.dateFormat.format(new Date())
	// + ".json";

	// public static String rampup_table = "tbl_rampup_process";

	public static HashMap<String, DomainWiseUserStats> preExistingHashMap_DomainWiseHashMap;

	public static HashMap<String, String> whitelable_table_map = new HashMap<String, String>();

	public static List<String> new_rampup_advertisement = new ArrayList<String>();

	public static String test_rampup_table = "tbl_rampup_process_testing";

	// ========================================//======================================

	public static Connection openConnection() {
		String HOST_NAME = "oakuserdbinstance-cluster.cluster-ca2bixk3jmwi.us-east-1.rds.amazonaws.com:3306";
		String DB_NAME = "oakalerts";
		String username = "awsoakuser";
		String password = "awsoakusersignity";
		String url1 = "jdbc:mysql://" + HOST_NAME + "/" + DB_NAME;
		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = (Connection) DriverManager.getConnection(url1, username,
					password);
		} catch (Exception e) {
			e.printStackTrace();

		}
		return con;
	}

	public static void closeConnection(Connection con) {
		try {
			con.close();
		} catch (Exception e) {
		}
	}

	public static String readComplete_jsonFromFile(String path) {

		InputStream in;
		StringBuilder out = new StringBuilder();
		try {
			in = new FileInputStream(new File(path));
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(in));

			String line;
			while ((line = reader.readLine()) != null) {
				out.append(line);
			}
			System.out.println(out.toString());

			reader.close();
			// Prints the string content
			// read
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// from input stream

		return out.toString();
	}

	public static List<RampupModel> convert(ResultSet rs) {
		List<RampupModel> localList = new ArrayList<RampupModel>();

		try {
			JSONArray array = new JSONArray();
			ResultSetMetaData rsmd = rs.getMetaData();

			while (rs.next()) {

				JSONObject jsonObj = new JSONObject();
				int numColumns = rsmd.getColumnCount();

				for (int i = 1; i < numColumns + 1; i++) {
					String column_name = rsmd.getColumnName(i);

					try {

						if (rsmd.getColumnType(i) == java.sql.Types.ARRAY) {
							jsonObj.put(column_name, rs.getArray(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.BIGINT) {
							jsonObj.put(column_name, rs.getInt(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.BOOLEAN) {
							jsonObj.put(column_name, rs.getBoolean(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.BLOB) {
							jsonObj.put(column_name, rs.getBlob(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.DOUBLE) {
							jsonObj.put(column_name, rs.getDouble(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.FLOAT) {
							jsonObj.put(column_name, rs.getFloat(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.INTEGER) {
							jsonObj.put(column_name, rs.getInt(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.NVARCHAR) {
							jsonObj.put(column_name, rs.getNString(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.VARCHAR) {
							jsonObj.put(column_name, rs.getString(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.TINYINT) {
							jsonObj.put(column_name, rs.getInt(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.SMALLINT) {
							jsonObj.put(column_name, rs.getInt(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.DATE) {
							jsonObj.put(column_name, rs.getDate(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.TIMESTAMP) {
							jsonObj.put(column_name,
									rs.getTimestamp(column_name));
						} else {
							jsonObj.put(column_name, rs.getObject(column_name));
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				array.put(jsonObj);

			}

			RampupModel[] rampUplist = new Gson().fromJson(array.toString(),
					RampupModel[].class);
			localList.addAll(Arrays.asList(rampUplist));

			return localList;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return localList;
	}

	// here we are reading the domain wise stats from the file

	public static String initialize_domain_wise_statst_map(String path) {

		InputStream in;
		StringBuilder out = new StringBuilder();
		try {
			in = new FileInputStream(new File(path));
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(in));

			String line;
			while ((line = reader.readLine()) != null) {
				out.append(line);
			}
			JSONObject jsonString = new JSONObject(out.toString());
			Utility.preExistingHashMap_DomainWiseHashMap = new Gson()
					.fromJson(
							jsonString.toString(),
							new TypeToken<HashMap<String, com.project.model.DomainWiseUserStats>>() {
							}.getType());

			reader.close();
			// Prints the string content
			// read

		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("File not found named by " + path);

		}

		// from input stream

		return out.toString();
	}

	// here we are adding informaion to the rampup model object
	public static void initilize_rampup_model_with_domainwise(RampupModel model) {

		Iterator it = Utility.preExistingHashMap_DomainWiseHashMap.entrySet()
				.iterator();
		System.out.println("===============================");
		while (it.hasNext()) {

			Map.Entry pair = (Map.Entry) it.next();
			// alumnirecruiter.com_bravemail.uncp.edu
			String domainKeyArray[] = pair.getKey().toString().split("_");

			DomainWiseUserStats localModel = Utility.preExistingHashMap_DomainWiseHashMap
					.get(pair.getKey());

			String whitelable = domainKeyArray[0].replace("-evening", "");
			String domain = domainKeyArray[1];
			// System.out.println("whitelable="+whitelable);

			// System.out.println(whitelable.toLowerCase() + " and "
			// + model.getProcessName().toLowerCase());

			if (whitelable.toLowerCase().contains(
					model.getProcessName().toLowerCase())) {

				Set<String> localSet = model.getWhitelable_domain_list().get(
						whitelable);
				if (localSet == null || localSet.size() == 0) {
					localSet = new HashSet<String>();
				}

				localSet.add(domain);

				HashMap<String, String> localMap = model.getDomain_wise_map()
						.get(domain);

				if (localMap == null)
					localMap = new HashMap<String, String>();

				String local_Count = localMap.get("Gmail");
				if (local_Count == null) {
					localMap.put("Gmail", localModel.getGmailCount());
				} else {
					localMap.put("Gmail", Integer.parseInt(local_Count)
							+ Integer.parseInt(localModel.getGmailCount()) + "");
				}
				local_Count = localMap.get("Yahoo");
				if (local_Count == null) {
					localMap.put("Yahoo", localModel.getYahooCount());
				} else {
					localMap.put("Yahoo", Integer.parseInt(local_Count)
							+ Integer.parseInt(localModel.getYahooCount()) + "");
				}
				local_Count = localMap.get("Aol");
				if (local_Count == null) {
					localMap.put("Aol", localModel.getAolCount());
				} else {
					localMap.put(
							"Aol",
							Integer.parseInt(local_Count)
									+ Integer.parseInt(localModel.getAolCount())
									+ "");
				}
				local_Count = localMap.get("Other");
				if (local_Count == null) {
					localMap.put("Other", localModel.getOthersCount());
				} else {
					localMap.put("Other", Integer.parseInt(local_Count)
							+ Integer.parseInt(localModel.getOthersCount())
							+ "");
				}

				model.getWhitelable_domain_list().put(whitelable, localSet);
				model.getDomain_wise_map().put(domain, localMap);

			}

			// it.remove(); // avoids a ConcurrentModificationException

		}

	}
}

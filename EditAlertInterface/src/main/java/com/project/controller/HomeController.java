package com.project.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.project.model.MyMainModel;
import com.project.model.RampupModel;
import com.project.model.UserModel;
import com.project.service.DataBaseServices;
import com.project.utility.Utility;

@Controller
@RequestMapping("/")
public class HomeController {

	@Autowired
	DataBaseServices dataBaseService;

	@RequestMapping(value = "/")
	public String editAlert() {
		System.out.println("in Edit Alert");
		return "login";
	}

	@RequestMapping(value = "/home")
	public String homePage() {
		System.out.println("in home page");
		return "home";
	}

	@RequestMapping(value = "/rampup-page")
	public String rampup_page() {
		System.out.println("in home page");
		return "rampup";
	}

	@RequestMapping(value = "/user-login", method = RequestMethod.GET)
	public @ResponseBody String checkUserCredentials(
			@RequestParam(value = "userName", defaultValue = "") String userName,
			@RequestParam(value = "userPassword", defaultValue = "") String userPassword,
			HttpServletResponse response, HttpServletRequest req

	) {

		System.out
				.println("userName=" + userName + " passeord=" + userPassword);
		String isUserValid = dataBaseService.checkUserCredentials(userName,
				userPassword);
		System.out.println(isUserValid);
		if (isUserValid.equalsIgnoreCase("true")) {
			HttpSession session = req.getSession(true);
			session.setAttribute("userName", userName);

		}

		return isUserValid;
	}

	@RequestMapping(value = "/rampup", method = RequestMethod.GET)
	public @ResponseBody List<RampupModel> getRampupDetail(
			@RequestParam("date") String date) {
		System.out.println("in edit rampup plan");

		if ((Utility.preExistingHashMap_DomainWiseHashMap == null) || 
			      (Utility.preExistingHashMap_DomainWiseHashMap.size() == 0)) {
			      System.out.println("rading domain wise file");
			      
			      Calendar cal = Calendar.getInstance();
			      cal.add(5, -1);
			      System.out.println(cal.getTime());
			      
			      Utility.domain_wise_file = Utility.domain_wise_file + 
			        Utility.dateFormat.format(cal.getTime()) + ".json";
			      
			      System.out.println(Utility.domain_wise_file);
			      
			      Utility.initialize_domain_wise_statst_map(Utility.domain_wise_file);
			    }
			    

			    dataBaseService.getWhitelable_table_map();
			    dataBaseService.get_new_rampup_advertisement();
			    
			    return dataBaseService.getRampupInformation(date);

	}

	@RequestMapping(value = "/rampup-update", method = RequestMethod.POST)
	public @ResponseBody boolean updateRampupInformation(
			@RequestBody List<RampupModel> list) {

		System.out.println("in updated method" + list.size());

		 return dataBaseService.updateRampupInformaton(list);

	}

	@RequestMapping(value = "/addnew-rampup", method = RequestMethod.POST)
	public @ResponseBody String addnewRampupRecord(
			@RequestBody RampupModel addModel) {

		System.out.println("in adding method" + addModel.getProcessName());

		return dataBaseService.addNewRecordInTable(addModel);

		
		// return dataBaseService.getRampupInformation(date);

	}

	@RequestMapping(value = "/search-user", method = RequestMethod.GET)
	public @ResponseBody List<UserModel> getProcessNames(
			@RequestParam("email") String email,
			@RequestParam(value = "keyword", defaultValue = "sales") String keyword,
			@RequestParam(value = "location", defaultValue = "10001") String location,
			@RequestParam(value = "frequency", defaultValue = "1") String frequency,
			@RequestParam(value = "radius") String radius,
			@RequestParam(value = "whitelabel", defaultValue = "cylcon") String whitelabel,
			@RequestParam(value = "providers", defaultValue = "") String providers

	) {

		System.out.println("providers" + providers);

		List<UserModel> processList = dataBaseService.getProcessNames(email,
				keyword, location, whitelabel, providers);
		return processList;
	}

	@RequestMapping(value = "/update-user", method = RequestMethod.GET)
	public @ResponseBody boolean updateUser(
			@RequestParam("email") String email,
			@RequestParam(value = "keyword", defaultValue = "sales") String keyword,
			@RequestParam(value = "zipcode", defaultValue = "10001") String zipcode,
			@RequestParam(value = "id") String id,
			@RequestParam(value = "city") String city,
			@RequestParam(value = "state") String state,
			@RequestParam(value = "frequency", defaultValue = "1") String frequency,
			@RequestParam(value = "radius") String radius,
			@RequestParam(value = "whitelabel") String whitelabel,
			@RequestParam(value = "table_name") String table_name

	) {

		boolean isUpdated = dataBaseService.updateUser(email, keyword, zipcode,
				id, frequency, radius, city, state, table_name);
		return isUpdated;
	}

	@RequestMapping(value = "/whitelables", method = RequestMethod.GET)
	public @ResponseBody MyMainModel getWhitelables() {

		// Utility.whitelables_tables_map_list =
		// dataBaseService.getWhitelables();
		Utility.oneTime_MainModel = dataBaseService
				.getAllComplete_Information(Utility.filePath);

		// return Utility.whitelables_tables_map_list;

		return Utility.oneTime_MainModel;
	}

	@RequestMapping(value = "/delete-user", method = RequestMethod.GET)
	public @ResponseBody boolean delete_User(
			@RequestParam("email") String email,
			@RequestParam(value = "id") String id,
			@RequestParam(value = "table_name") String table_name

	) {

		boolean isDeleted = dataBaseService.delete_User_info(email, id,
				table_name);
		return isDeleted;
	}

	@RequestMapping(value = "/complete-user-search", method = RequestMethod.GET)
	public @ResponseBody List<UserModel> getCompleteUserInfo(
			@RequestParam("email") String email,
			@RequestParam(value = "whitelabel", defaultValue = "cylcon") String whitelabel,
			@RequestParam(value = "providers", defaultValue = "") String providers

	) {

		System.out.println("providers" + providers);

		List<UserModel> processList = dataBaseService
				.getUserInfoFromCompleteDb(email, whitelabel, providers);
		return processList;
	}

}

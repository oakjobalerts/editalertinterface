package com.project.service.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.gson.Gson;
import com.mysql.jdbc.Statement;
import com.project.model.LoginModel;
import com.project.model.MyMainModel;
import com.project.model.RampupModel;
import com.project.model.UserModel;
import com.project.service.DataBaseServices;
import com.project.utility.Utility;

@Repository
public class DataBaseServiceImpl implements DataBaseServices {

	@Autowired
	SessionFactory sessionFactory;

	@Transactional
	public List<UserModel> getProcessNames(String email, String keyword,
			String location, String whiteLable, String providers) {
		// TODO Auto-generated method stub
		ArrayList<UserModel> list = new ArrayList<UserModel>();

		String whiteLalename = whiteLable;
		System.out.println(whiteLable);
		String tableString = "";
		String tablesArray[];
		Set<String> tableSet = new HashSet<String>();

		// meand searching only based on the whitelables
		if (providers != null && providers.equalsIgnoreCase("")) {

			tableString = Utility.oneTime_MainModel.getWhitelabel_table_map()
					.get(whiteLable);
			tablesArray = tableString.split("\\|");

			for (int i = 0; i < tablesArray.length; i++) {
				tableSet.add(tablesArray[i]);

			}

		} else {

			String advertisement_String_array[] = providers.split(",");

			for (int i = 0; i < advertisement_String_array.length; i++) {
				System.out.println(advertisement_String_array[i]);

				tableString = Utility.oneTime_MainModel.getAdvertisement_map()
						.get(advertisement_String_array[i]);
				System.out.println(tableString);
				try {
					tablesArray = tableString.split("\\|");

					for (int j = 0; j < tablesArray.length; j++) {
						tableSet.add(tablesArray[j]);

					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
				}

			}

		}

		System.out.println("tableSet=" + tableSet.size());

		for (String tableName : tableSet) {
			boolean isForWeb = false;
			String query = "";
			if (tableName.contains("whitelabels")
					|| tableName.equalsIgnoreCase("tbl_jobAlerts")) {

				query = "select id,emailAddress,keyword,location,radius,frequency from "
						+ tableName + " where emailAddress='" + email + "'";

				isForWeb = true;

			} else {

				query = "select id,email,keyword,zipcode,city,state,radius,frequency from "
						+ tableName + " where email='" + email + "'";

			}

			System.out.println(query);

			Query sqlQuery = getSessionFactory().createSQLQuery(query);
			List<Object[]> rows = sqlQuery.list();
			int index = 0;
			for (Object[] row : rows) {

				try {
					UserModel userModel = new UserModel();
					userModel.user_id = row[0].toString();
					userModel.email = row[1].toString();
					userModel.keyword = row[2].toString();
					userModel.location = row[3].toString();
					if (isForWeb) {
						if (userModel.location.contains(",")) {

							String ar[] = userModel.location.split(",");
							userModel.city = ar[0].toString();
							userModel.state = ar[1].toString();
						}

						userModel.radius = row[4].toString();
						userModel.frequency = row[5].toString();

					} else {
						userModel.city = row[4].toString();
						userModel.state = row[5].toString();
						userModel.radius = row[6].toString();
						userModel.frequency = row[7].toString();
					}
					userModel.user_table_name = tableName.trim();
					userModel.documentId = "" + index;
					index++;
					list.add(userModel);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return list;
	}

	public Session getSessionFactory() {
		return sessionFactory.getCurrentSession();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	@Transactional
	public boolean updateUser(String email, String keyword, String zipCode,
			String id, String frequency, String radius, String city,
			String state, String table_name) {
		// TODO Auto-generated method stub

		boolean isForWeb = false;
		String query = "";
		table_name = table_name;

		if (frequency.equalsIgnoreCase("daily")) {
			frequency = "1";

		} else if (frequency.equalsIgnoreCase("weekly")) {
			frequency = "2";

		} else if (frequency.equalsIgnoreCase("monthly")) {
			frequency = "3";

		}

		if (table_name.contains("whitelabels")
				|| table_name.equalsIgnoreCase("tbl_jobAlerts")) {
			isForWeb = true;
		}

		if (isForWeb) {

			query = "update " + table_name + " set emailAddress='" + email
					+ "' , keyword='" + keyword + "', location='" + zipCode
					+ "' , frequency='" + frequency + "'" + " , radius='"
					+ radius + "' where id=" + id;

		} else {

			query = "update " + table_name + " set email='" + email
					+ "' , keyword='" + keyword + "', zipcode='" + zipCode
					+ "' , frequency='" + frequency + "'" + " , radius='"
					+ radius + "' , city='" + city + "' , state='" + state
					+ "' where id=" + id;
		}

		System.out.println(query);

		Connection con = Utility.openConnection();

		if (con != null) {

			java.sql.Statement st = null;
			try {
				st = con.createStatement();
				int isUpdted = st.executeUpdate(query);

				System.out.println(isUpdted);

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			;
		}

		try {
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}

	@Override
	@Transactional
	public Map<String, List<String>> getWhitelables() {
		// TODO Auto-generated method stub
		Map<String, List<String>> table_hashMap = new HashMap<String, List<String>>();

		String query = "select whitelabel_name,table_name from whitelabels_process group by 1";

		Query sqlQuery = getSessionFactory().createSQLQuery(query);
		List<Object[]> rows = sqlQuery.list();

		for (Object[] row : rows) {
			try {
				List<String> list = new ArrayList<String>();

				String whitelable = row[0].toString();

				String tableNames = row[1].toString();

				String[] tableArray = tableNames.split("\\|");

				for (int i = 0; i < tableArray.length; i++) {
					String table_name = tableArray[i].toString();
					list.add(table_name);
				}

				table_hashMap.put(whitelable, list);

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		return table_hashMap;
	}

	@Override
	@Transactional
	public String checkUserCredentials(String userName, String userPassword) {
		// TODO Auto-generated method stub
		if (userName.equalsIgnoreCase(LoginModel.userName)
				&& userPassword.equalsIgnoreCase(LoginModel.userPassword)) {
			System.out.println("successfully login");
			return "true";
		} else if (userName.equalsIgnoreCase(LoginModel.userName_rampup)
				&& userPassword
						.equalsIgnoreCase(LoginModel.userPassword_rampup)) {

			return "rampup";

		} else {
			System.out.println("successfully not login");

			return "false";

		}

	}

	@Override
	@Transactional
	public MyMainModel getAllComplete_Information(String file_path) {
		// TODO Auto-generated method stub
		String finalResult = Utility.readComplete_jsonFromFile(file_path);
		Gson gson = new Gson();
		MyMainModel[] data = gson.fromJson(finalResult, MyMainModel[].class);
		Utility.oneTime_MainModel = data[0];

		return Utility.oneTime_MainModel;
	}

	@Override
	public boolean delete_User_info(String email, String id, String table_name) {
		// TODO Auto-generated method stub
		boolean is_deleted = false;
		String delete_query = "";

		// if (table_name.equalsIgnoreCase("tbl_jobAlerts_whitelabels")
		// || table_name.equalsIgnoreCase("tbl_jobAlerts_whitelabels")) {
		// delete_query = "delete from " + table_name + " where id='" + id
		// + "' and emailAddress='" + email + "'";
		//
		// } else {
		// delete_query = "delete from " + table_name + " where id='" + id
		// + "' and email='" + email + "'";
		//
		// }

		delete_query = "delete from " + table_name + " where id='" + id + "'";

		System.out.println("delete_query " + delete_query);

		Connection con = Utility.openConnection();

		if (con != null) {

			try {
				java.sql.Statement st = con.createStatement();

				is_deleted = st.execute(delete_query);
				System.out.println("is_deleted=" + is_deleted);

				if (!is_deleted) {
					return true;
				}

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		return is_deleted;
	}

	@Transactional
	@Override
	public List<UserModel> getUserInfoFromCompleteDb(String email,
			String whitelabel, String providers) {
		// TODO Auto-generated method stub
		if (Utility.oneTime_MainModel == null) {
			getAllComplete_Information(Utility.filePath);

		}
		ArrayList<UserModel> list = new ArrayList<UserModel>();
		String allTableString = "";

		if (whitelabel.equalsIgnoreCase("all")) {
			allTableString = Utility.oneTime_MainModel.getAll_table_map().get(
					"AllTable");

		} else {
			System.out.println("search for " + whitelabel + "");
			allTableString = Utility.oneTime_MainModel
					.getWhitelabel_table_map().get(whitelabel);

		}
		// ==========================================//================================
		if (allTableString == null) {
			allTableString = "";
			getAllComplete_Information(Utility.filePath);
			System.out.println("search for " + whitelabel + "");

			if (whitelabel.equalsIgnoreCase("all")) {
				allTableString = Utility.oneTime_MainModel.getAll_table_map()
						.get("AllTable");
			} else {
				System.out.println("search for " + whitelabel + "");
				allTableString = Utility.oneTime_MainModel
						.getWhitelabel_table_map().get(whitelabel);

				allTableString = allTableString.replaceAll(
						"tbl_jobAlerts_whitelabels", "");
				allTableString = allTableString.replaceAll("tbl_jobAlerts", "");

			}

		}
		System.out.println("allTableString=" + allTableString);

		allTableString = allTableString.replaceAll("tbl_jobAlerts_whitelabels",
				"");
		allTableString = allTableString.replaceAll("tbl_jobAlerts", "");

		allTableString = allTableString
				+ "|tbl_jobAlerts_whitelabels|tbl_jobAlerts";

		String[] tablesArray = allTableString.split("\\|");

		for (int i = 0; i < tablesArray.length; i++) {
			String tableName = tablesArray[i];
			if (tableName.equalsIgnoreCase(""))
				continue;
			String whitelableName = Utility.oneTime_MainModel
					.getTable_whitelabel_map().get(tableName);

			boolean isForWeb = false;
			String query = "";
			if (tableName.contains("whitelabels")
					|| tableName.equalsIgnoreCase("tbl_jobAlerts")) {

				query = "select id,emailAddress,keyword,location,radius,frequency,provider from "
						+ tableName + " where emailAddress='" + email + "'";
				isForWeb = true;

			} else if (tableName.equalsIgnoreCase("tbl_jobvital_seeker")) {
				query = "select id,email,keyword,location,city,state,radius,frequency from "
						+ tableName + " where email='" + email + "'";

			} else {

				query = "select id,email,keyword,zipcode,city,state,radius,frequency from "
						+ tableName + " where email='" + email + "'";

			}

			Query sqlQuery = getSessionFactory().createSQLQuery(query);
			List<Object[]> rows = sqlQuery.list();
			int index = 0;
			String mainIndex = "" + index + i;
			for (Object[] row : rows) {

				mainIndex = "" + index + i;
				try {
					UserModel userModel = new UserModel();
					userModel.user_id = row[0].toString();
					userModel.email = row[1].toString();
					userModel.keyword = row[2].toString();
					userModel.location = row[3].toString();
					if (isForWeb) {
						whitelableName = "Web";
						if (userModel.location.contains(",")) {

							String ar[] = userModel.location.split(",");
							userModel.city = ar[0].toString();
							userModel.state = ar[1].toString();
						}

						userModel.radius = row[4].toString();
						userModel.frequency = row[5].toString();

						if (row[6].toString().equalsIgnoreCase("web")) {
							whitelableName = whitelableName + "-oakjob";
						} else {
							whitelableName = whitelableName + "-"
									+ row[6].toString();
						}
					} else {
						userModel.city = row[4].toString();
						userModel.state = row[5].toString();
						userModel.radius = row[6].toString();
						userModel.frequency = row[7].toString();
					}

					userModel.user_table_name = tableName.trim();
					userModel.documentId = "" + mainIndex;

					userModel.user_whitelable = whitelableName;

					list.add(userModel);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}
		return list;
	}

	@Override
	public List<RampupModel> getRampupInformation(String date) {
		// TODO Auto-generated method stub

		String query = "";

		query = "select * from " + Utility.test_rampup_table
				+ " where date(date)= '" + date + "'";

		System.out.println("rampup-query= " + query);

		Connection con = Utility.openConnection();

		try {
			if (con != null && !con.isClosed()) {

				java.sql.Statement st = con.createStatement();

				ResultSet res = st.executeQuery(query);

				List<RampupModel> localList = Utility.convert(res);

				for (int i = 0; i < localList.size(); i++) {
					RampupModel model = localList.get(i);
					model.rampup_types.add("Fixed");
					model.rampup_types.add("MailGun");
					model.user_domain_type.add("All");
					model.user_domain_type.add("Gmail");
					model.user_domain_type.add("Non Gmail");
					model.user_domain_type.add("Aol");
					model.user_domain_type.add("Hotmail");
					model.ramp_status_list.add("Active");
					model.ramp_status_list.add("InActive");
					model.setObject_position("" + i);

					Utility.initilize_rampup_model_with_domainwise(model);

					model.setWhitelable_table_map(Utility.whitelable_table_map);
					model.setNew_rampup_advertisement(Utility.new_rampup_advertisement);

					System.out.println(model.getDomain_wise_map().size());

					localList.set(i, model);

				}
				return localList;

			}
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new ArrayList<RampupModel>();
	}

	// this is use to initialize the whitlable with table map
	@Override
	public HashMap<String, String> getWhitelable_table_map() {
		// TODO Auto-generated method stub

		String query = "";

		query = "select whitelabel_name,table_name from whitelabels_process where whitelabel_name is not null group by 1";

		System.out.println("rampup-query= " + query);

		Connection con = Utility.openConnection();

		try {
			if (con != null && !con.isClosed()) {

				java.sql.Statement st = con.createStatement();

				ResultSet res = st.executeQuery(query);

				while (!res.isClosed() && res.next()) {

					String whitelable = res.getString("whitelabel_name");
					String tableArray[] = res.getString("table_name").split(
							"\\|");
					String table = "";

					if (tableArray.length > 1) {
						table = tableArray[1].trim();
					} else {
						table = tableArray[0].trim();
					}

					Utility.whitelable_table_map.put(whitelable, table);

				}

			}
			if (con != null) {
				con.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	// this is use to initialize the rampup with table map
	@Override
	public List<String> get_new_rampup_advertisement() {
		// TODO Auto-generated method stub

		String query = "";

		query = "select advertisement from tbl_job_seekers_common_import where advertisement!='' and (advertisement=advertisement_for_code or advertisement_for_code='') group by 1";

		System.out.println("rampup-query= " + query);

		Connection con = Utility.openConnection();

		try {
			if (con != null && !con.isClosed()) {

				java.sql.Statement st = con.createStatement();

				ResultSet res = st.executeQuery(query);

				while (!res.isClosed() && res.next()) {

					String advertisement = res.getString("advertisement");

					Utility.new_rampup_advertisement.add(advertisement);

				}

			}
			if (con != null) {
				con.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public String addNewRecordInTable(RampupModel model) {
		// TODO Auto-generated method stub

		String query = "";
		String id="0";
		String from_table_name = "tbl_job_seekers_common_import";
		model.setRamp_up_day_count("0");
		model.setIs_advertise_use(true);
		model.setIs_rampup_live("1");
		model.setType_of_users("All");
		model.setIs_move_or_copy("M");
		model.setIs_priority_use(false);
		model.setDatecondition_use(false);
		model.setDayCount("0");

		if (model.getProcessName().toLowerCase().contains("handpick")) {
			from_table_name = "tbl_job_seekers_handpickedjobs_rampup";
		}

		query = "INSERT INTO "
				+ Utility.test_rampup_table
				+ " (processName, from_table_name, to_table_name, ramp_users_count, ramp_up_day_count, ramp_up_type, rampup_advertisement, is_advertise_use, is_rampup_live,date"
				+ ",type_of_users,is_move_or_copy,starting_index,percent,is_priority_use,rampup_priority,isDatecondition_use,dayCount,user_domain,domain_type"
				+ ")" + "VALUES (" + "'" + model.getProcessName() + "','"
				+ from_table_name + "','"
				+ model.getWhitelable_table_map().get(model.getProcessName())
				+ "','" + model.getRamp_users_count() + "','"
				+ model.getRamp_up_day_count() + "','"
				+ model.getRamp_up_type() + "','"
				+ model.getRampup_advertisement() + "','"
				+ model.isIs_advertise_use() + "','"
				+ model.getIs_rampup_live() + "','" + model.getDate() + "','"
				+ model.getType_of_users() + "','" + model.getIs_move_or_copy()
				+ "','" + "0" + "','" + "0" + "'" + ",'"
				+ model.isIs_priority_use() + "','"
				+ model.getRampup_priority() + "','"
				+ model.isDatecondition_use() + "','" + model.getDayCount()
				+ "','" + model.getSelected_user_domain_type() + "','"
				+ model.getSelected_user_domain_type() + "'" + ")";

		System.out.println("rampup-insert query= " + query);

		Connection con = Utility.openConnection();

		try {
			if (con != null && !con.isClosed()) {

				java.sql.Statement st = con.createStatement();

				int insert_new_Record_intable_result = st.executeUpdate(query);

				if (insert_new_Record_intable_result != 0) {

					System.out.println("Successfully inserted");
					String q="select id from "+Utility.test_rampup_table+" order by id desc limit 1";
					ResultSet res = st.executeQuery(q);
					
					while (!res.isClosed() && res.next()) {

						 id = res.getString("id");
					}
					if(con!=null){
						con.close();
					}
					return id;
				}

			}

		} catch (Exception e) {
			e.printStackTrace();

		}

		return id;
	}

	@Override
	public boolean updateRampupInformaton(List<RampupModel> list) {
		// TODO Auto-generated method stub

		if (list != null && list.size() != 0) {

			Connection con = Utility.openConnection();

			try {
				if (con != null && !con.isClosed()) {
					Statement st = (Statement) con.createStatement();

					for (RampupModel rampupModel : list) {
						if (rampupModel.getSelected_rampup_status()
								.equalsIgnoreCase("InActive")) {
							rampupModel.setIs_rampup_live("0");
						} else {
							rampupModel.setIs_rampup_live("1");
						}

						String update_lastRecord_query = "update "
								+ Utility.test_rampup_table
								+ " set ramp_up_type='"
								+ rampupModel.getRamp_up_type()
								+ "',ramp_users_count='"
								+ rampupModel.getRamp_users_count()
								+ "',is_rampup_live='"
								+ rampupModel.getIs_rampup_live()
								+ "',domain_type='"
								+ rampupModel.getDomain_type()
								+ "'  where id='" + rampupModel.getId()
								+ "' and date(date)='"
								+ rampupModel.getDate().split(" ")[0] + "'";

						System.out.println(update_lastRecord_query);

						int isUpdated = st
								.executeUpdate(update_lastRecord_query);
						// upating the record
						if (isUpdated != 0) {
							System.out.println("Successfully updated");
						}
						
						if(con!=null){
							con.close();
						}

					}
					return true;
				}
			} catch (Exception e) {

			}

			return false;
		}
		return false;
	}

}

package com.project.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.project.model.MyMainModel;
import com.project.model.RampupModel;
import com.project.model.UserModel;

public interface DataBaseServices {

	public List<UserModel> getProcessNames(String email, String keyword,
			String location, String whiteLable, String providers);

	public boolean updateUser(String email, String keyword, String zipCode,
			String id, String frequency, String radius, String city,
			String state, String table_name);

	public Map<String, List<String>> getWhitelables();

	public String checkUserCredentials(String email, String keyword);

	public MyMainModel getAllComplete_Information(String file_path);

	public boolean delete_User_info(String email, String id, String table_name);

	public List<UserModel> getUserInfoFromCompleteDb(String email,
			String whitelabel, String providers);

	public List<RampupModel> getRampupInformation(String date);

	public HashMap<String, String> getWhitelable_table_map();

	public List<String> get_new_rampup_advertisement();

	public String addNewRecordInTable(RampupModel newModel);

	public boolean updateRampupInformaton(List<RampupModel> list);

}

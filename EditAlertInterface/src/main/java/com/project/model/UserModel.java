package com.project.model;

public class UserModel {

	public String email = "";
	public String keyword = "";
	public String location = "";
	public String frequency = "";
	public String radius = "";
	public String city = "";
	public String state = "";
	public String user_table_name = "";
	public String documentId="";
	public String user_whitelable="";
	

	public String getUser_whitelable() {
		return user_whitelable;
	}

	public void setUser_whitelable(String user_whitelable) {
		this.user_whitelable = user_whitelable;
	}

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public String getUser_table_name() {
		return user_table_name;
	}

	public void setUser_table_name(String user_table_name) {
		this.user_table_name = user_table_name;
	}

	public String user_id = "";
	public String whiteLable = "";

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getWhiteLable() {
		return whiteLable;
	}

	public void setWhiteLable(String whiteLable) {
		this.whiteLable = whiteLable;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public String getRadius() {
		return radius;
	}

	public void setRadius(String radius) {
		this.radius = radius;
	}

}

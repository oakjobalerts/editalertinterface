package com.project.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class RampupModel {

	private int id;
	private String processName = "";
	private String from_table_name = "";
	private String to_table_name = "";
	private String ramp_users_count = "";
	private String ramp_up_day_count = "";
	private String date = "";
	private String ramp_up_type = "";
	private String rampup_advertisement = "";
	private boolean is_advertise_use = false;
	private String is_rampup_live = "0";
	private boolean today_rampup_status = false;
	private String type_of_users = "ALL";
	private String is_move_or_copy = "C";
	private String provider = "";
	private boolean is_priority_use = false;
	private Integer rampup_priority = 0;
	private boolean isDatecondition_use = false;
	private String dayCount = "";
	private String user_domain = "";
	private String selected_ramp_type = "";
	private String selected_user_domain_type = "";
	private String domain_type = "";
	private String object_position="0";


	public String getObject_position() {
		return object_position;
	}

	public void setObject_position(String object_position) {
		this.object_position = object_position;
	}

	private HashMap<String, String> whitelable_table_map = new HashMap<String, String>();
	private List<String> new_rampup_advertisement = new ArrayList<String>();

	public List<String> getNew_rampup_advertisement() {
		return new_rampup_advertisement;
	}

	public void setNew_rampup_advertisement(
			List<String> new_rampup_advertisement) {
		this.new_rampup_advertisement = new_rampup_advertisement;
	}

	public String getSelected_rampup_status() {
		return selected_rampup_status;
	}

	public HashMap<String, String> getWhitelable_table_map() {
		return whitelable_table_map;
	}

	public void setWhitelable_table_map(
			HashMap<String, String> whitelable_table_map) {
		this.whitelable_table_map = whitelable_table_map;
	}

	public void setSelected_rampup_status(String selected_rampup_status) {
		this.selected_rampup_status = selected_rampup_status;
	}

	private HashMap<String, Set<String>> whitelable_domain_list = new HashMap<String, Set<String>>();

	private HashMap<String, HashMap<String, String>> domain_wise_map = new HashMap<String, HashMap<String, String>>();

	public List<String> ramp_status_list = new ArrayList<String>();
	private String selected_rampup_status = "";

	public List<String> getRamp_status_list() {
		return ramp_status_list;
	}

	public void setRamp_status_list(List<String> ramp_status_list) {
		this.ramp_status_list = ramp_status_list;
	}

	public String getSelected_user_domain_type() {
		return selected_user_domain_type;
	}

	public HashMap<String, Set<String>> getWhitelable_domain_list() {
		return whitelable_domain_list;
	}

	public void setWhitelable_domain_list(
			HashMap<String, Set<String>> whitelable_domain_list) {
		this.whitelable_domain_list = whitelable_domain_list;
	}

	public HashMap<String, HashMap<String, String>> getDomain_wise_map() {
		return domain_wise_map;
	}

	public void setDomain_wise_map(
			HashMap<String, HashMap<String, String>> domain_wise_map) {
		this.domain_wise_map = domain_wise_map;
	}

	public void setSelected_user_domain_type(String selected_user_domain_type) {
		this.selected_user_domain_type = selected_user_domain_type;
	}

	public String getDomain_type() {
		return domain_type;
	}

	public void setDomain_type(String domain_type) {
		this.domain_type = domain_type;
	}

	private String isUpdateRequired = "false";

	public String getSelected_ramp_type() {
		return selected_ramp_type;
	}

	public void setSelected_ramp_type(String selected_ramp_type) {
		this.selected_ramp_type = selected_ramp_type;
	}

	public String getIsUpdateRequired() {
		return isUpdateRequired;
	}

	public void setIsUpdateRequired(String isUpdateRequired) {
		this.isUpdateRequired = isUpdateRequired;
	}

	public List<String> getRampup_types() {
		return rampup_types;
	}

	public void setRampup_types(List<String> rampup_types) {
		this.rampup_types = rampup_types;
	}

	public List<String> rampup_types = new ArrayList<String>();

	public List<String> user_domain_type = new ArrayList<String>();

	public List<String> getUser_domain_type() {
		return user_domain_type;
	}

	public void setUser_domain_type(List<String> user_domain_type) {
		this.user_domain_type = user_domain_type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public String getFrom_table_name() {
		return from_table_name;
	}

	public void setFrom_table_name(String from_table_name) {
		this.from_table_name = from_table_name;
	}

	public String getTo_table_name() {
		return to_table_name;
	}

	public void setTo_table_name(String to_table_name) {
		this.to_table_name = to_table_name;
	}

	public String getRamp_users_count() {
		return ramp_users_count;
	}

	public void setRamp_users_count(String ramp_users_count) {
		this.ramp_users_count = ramp_users_count;
	}

	public String getRamp_up_day_count() {
		return ramp_up_day_count;
	}

	public void setRamp_up_day_count(String ramp_up_day_count) {
		this.ramp_up_day_count = ramp_up_day_count;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getRamp_up_type() {
		return ramp_up_type;
	}

	public void setRamp_up_type(String ramp_up_type) {
		this.ramp_up_type = ramp_up_type;
	}

	public String getRampup_advertisement() {
		return rampup_advertisement;
	}

	public void setRampup_advertisement(String rampup_advertisement) {
		this.rampup_advertisement = rampup_advertisement;
	}

	public boolean isIs_advertise_use() {
		return is_advertise_use;
	}

	public void setIs_advertise_use(boolean is_advertise_use) {
		this.is_advertise_use = is_advertise_use;
	}

	public String getIs_rampup_live() {
		return is_rampup_live;
	}

	public void setIs_rampup_live(String is_rampup_live) {
		this.is_rampup_live = is_rampup_live;
	}

	public boolean isToday_rampup_status() {
		return today_rampup_status;
	}

	public void setToday_rampup_status(boolean today_rampup_status) {
		this.today_rampup_status = today_rampup_status;
	}

	public String getType_of_users() {
		return type_of_users;
	}

	public void setType_of_users(String type_of_users) {
		this.type_of_users = type_of_users;
	}

	public String getIs_move_or_copy() {
		return is_move_or_copy;
	}

	public void setIs_move_or_copy(String is_move_or_copy) {
		this.is_move_or_copy = is_move_or_copy;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public boolean isIs_priority_use() {
		return is_priority_use;
	}

	public void setIs_priority_use(boolean is_priority_use) {
		this.is_priority_use = is_priority_use;
	}

	public Integer getRampup_priority() {
		return rampup_priority;
	}

	public void setRampup_priority(Integer rampup_priority) {
		this.rampup_priority = rampup_priority;
	}

	public boolean isDatecondition_use() {
		return isDatecondition_use;
	}

	public void setDatecondition_use(boolean isDatecondition_use) {
		this.isDatecondition_use = isDatecondition_use;
	}

	public String getDayCount() {
		return dayCount;
	}

	public void setDayCount(String dayCount) {
		this.dayCount = dayCount;
	}

	public String getUser_domain() {
		return user_domain;
	}

	public void setUser_domain(String user_domain) {
		this.user_domain = user_domain;
	}

}

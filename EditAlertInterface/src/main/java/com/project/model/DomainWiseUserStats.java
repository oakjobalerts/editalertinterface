package com.project.model;

import java.util.HashMap;

public class DomainWiseUserStats {

	private String procesSName;
	private String gmailCount;
	private String yahooCount;
	private String aolCount;
	private String othersCount;

	public String getGmailCount () {
		return gmailCount;
	}

	public void setGmailCount (String gmailCount) {
		this.gmailCount = gmailCount;
	}

	public String getYahooCount () {
		return yahooCount;
	}

	public void setYahooCount (String yahooCount) {
		this.yahooCount = yahooCount;
	}

	public String getAolCount () {
		return aolCount;
	}

	public void setAolCount (String aolCount) {
		this.aolCount = aolCount;
	}

	public String getOthersCount () {
		return othersCount;
	}

	public void setOthersCount (String othersCount) {
		this.othersCount = othersCount;
	}

	public String getProcesSName () {
		return procesSName;
	}

	public void setProcesSName (String procesSName) {
		this.procesSName = procesSName;
	}

	

}

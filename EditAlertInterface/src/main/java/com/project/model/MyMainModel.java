package com.project.model;

import java.util.HashMap;
import java.util.Map;

public class MyMainModel {

	public Map<String, String> whitelabel_map = new HashMap<String, String>();
	public Map<String, String> advertisement_map = new HashMap<String, String>();
	public Map<String, String> whitelabel_table_map = new HashMap<String, String>();
	public Map<String, String> table_whitelabel_map = new HashMap<String, String>();

	public Map<String, String> all_table_map = new HashMap<String, String>();

	public Map<String, String> getTable_whitelabel_map() {
		return table_whitelabel_map;
	}

	public void setTable_whitelabel_map(Map<String, String> table_whitelabel_map) {
		this.table_whitelabel_map = table_whitelabel_map;
	}

	public Map<String, String> getAll_table_map() {
		return all_table_map;
	}

	public void setAll_table_map(Map<String, String> all_table_map) {
		this.all_table_map = all_table_map;
	}

	public Map<String, String> getWhitelabel_table_map() {
		return whitelabel_table_map;
	}

	public void setWhitelabel_table_map(Map<String, String> whitelabel_table_map) {
		this.whitelabel_table_map = whitelabel_table_map;
	}

	public Map<String, String> getWhitelabel_map() {
		return whitelabel_map;
	}

	public void setWhitelabel_map(Map<String, String> whitelabel_map) {
		this.whitelabel_map = whitelabel_map;
	}

	public Map<String, String> getAdvertisement_map() {
		return advertisement_map;
	}

	public void setAdvertisement_map(Map<String, String> advertisement_map) {
		this.advertisement_map = advertisement_map;
	}

}
